<?php  

namespace Cart;

use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Response;



/**
 * 
 */
class Cart 
{
	public $cart_class = [];
	public $session;

	function __construct()
	{
		$this->session = new Session();
		if (!$this->session->has('cart_session')) {
			$this->cart_class = 
			[
				'total_items' => 0,
				'total_cart' => 0,
			];
			$this->session->set('cart_session', $this->cart_class);
		}

		$this->cart_class = $this->session->get('cart_session');
	}


	/**
	 * Function add item cart
	 */
	public function addItem($item)
	{
		if (!is_array($item) || count($item) === 0) {
			return;
		}
		if (!$item['qty']) {
			return;
		}
		$key = uniqid($item['id']);
		$this->cart_class[$key] = $item;
	}

	/**
     * 
	 */
	public function save()
	{
		$this->cart_class['total_items'] = $this->cart_class['total_cart'] = 0;
		$total_cart = 0; 
		$total_items = 0;
		// dump($this->cart_class);die;
		foreach ($this->cart_class as $key) {
			if (!is_array($key)) {
				continue;
			}
			/*Si existe descuento lo suma al total*/
			if ($key['sales'] > 0 || $key['sales'] =! null) {
				$total_cart += $key['saleprice'] * $key['qty']; 
			}else{
				$total_cart += $key['price'] * $key['qty'];
			}
			
			$total_items += $key['qty'];	
		}


		$this->cart_class['total_cart'] = $total_cart;
		$this->cart_class['total_items'] = $total_items;
		$this->session->set('cart_session', $this->cart_class);
		$this->session->getFlashBag()->add('success', 'Item added to cart');

	}

	/**
     * Funcion que retorna el precio con descuento
	 */
	public function calcSale($sale,$price)
	{
		return $price - ($sale * $price) / 100;
	}

	/*Hacer Metodos*/

	// contents
	// get_item
	// total_items
	// total
	// update
	// destroy
	//remove_item

	public function contents()
	{
		$cart = $this->cart_class;
		unset($cart['total_cart']);
		unset($cart['total_items']);

		return array_reverse($cart);
	}

	public function total_items()
	{
		if (!$this->cart_class > 2) {
			return 0;
		}
		return $this->cart_class['total_items'];
	}

	public function total()
	{
		return $this->cart_class['total_cart'];
	}


	public function remove_item($key)
	{
		// dump(array_key_exists($key, $this->cart_class));die;
		if (count($this->cart_class) < 3) {
			return false;
		}

		if (!array_key_exists($key, $this->cart_class)) {
			return 'false';
		}
		
		unset($this->cart_class[$key]);
		/*Actualiza el array de items*/
		$this->save();
		$this->session->getFlashBag()->add('deleted', 'Deleted Item');
		return 'true';

	}

	public function get_item($key)
	{
		return  array_key_exists($key, $this->cart_class) 
		? $this->cart_class[$key] 
		: false;
	}

	public function destroy()
	{
		/*Verificacion si posee articulos*/
		if (!count($this->cart_class) > 2 && !$this->session->has('cart_session')) {
			return false;
		}

		unset($this->cart_class);
		$this->session->remove('cart_session');

	}
	
}