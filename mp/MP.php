<?php  
namespace MP;

use Symfony\Component\HttpFoundation\Response;
use Cart\Cart;

/**
 * 
 */
class MP 
{	
	/*Clase utilizada con la API*/
	public function payment($request)
	{
		$cart = new Cart();
		\MercadoPago\SDK::setAccessToken($_ENV['MP_ACCESS_TOKEN']);
		
		/*Datos de Pago*/
		$payment = new \MercadoPago\Payment();
		$payment->transaction_amount = (float)$request['transaction_amount'];
		$payment->token = $request['token'];
		$payment->description = $request['description'];
		$payment->installments = (int)$request['installments'];
		$payment->payment_method_id = $request['payment_method_id'];
		$payment->issuer_id = (int)$request['issuer_id'];
		$payment->external_reference = "MP2021";

		/*Datos de comprador*/
		$payer = new \MercadoPago\Payer();
		$payer->email = $request['payer']['email'];
		$payer->identification = array(
			"type" => $request['payer']['identification']['type'],
			"number" => $request['payer']['identification']['number'],
		);
		$payment->payer = $payer;
		$payment->save();	

		$response = array(
			'status' => $payment->status,
			'status_detail' => $payment->status_detail,
			'id' => $payment->id,
			'external_reference' => $payment->external_reference,
			'payer_id' => $payment->payer->id,
			'date_created' => $payment->date_created,
		);
		
		return $response;	
	}
}