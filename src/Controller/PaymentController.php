<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Payment;
use App\Entity\Order;
use App\Entity\Receipt;
use Cart\Cart;
use App\Entity\OrderHasProducts;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

/*Biblioteca creacion PDF*/
use Dompdf\Dompdf;

class PaymentController extends AbstractController
{

    /*Array con datos del pago efectuado*/
    protected $data_payment;

    /*Inicializacion de Payment Entity*/
    protected $payment;

    protected $mailer;

    function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

   /**
    * @Route("/payment/{payment_id?}", name="payment", methods={"GET", "PUT"})
    */
   public function index(Request $request, $payment_id = null):Response
   {
        $this->denyAccessUnlessGranted('ROLE_USER'); 
        /*Ingresa por URL con intencion de crear*/
        if ((null == $payment_id) && (null != $request->query->get('payment_id'))) {
            /*Ingresa luego del pago por URL*/
            if (!$this->exist($request->query->get('payment_id'))) {
                // dump($request); die;
                $em = $this->getDoctrine()->getManager();
                /**probando adaptacion*/    
                $payment = new Payment($request->query->get('payment_id'));
                /***/
                $em->persist($payment);
                $em->flush();
                /*Creacion de Orden*/
                $order = new Order();
                $data_order = 
                    [
                        'user' => $this->getUser(),
                        'payment' => $payment,
                    ];
                /*************************************************Creacion Factura y Envio adjunto por Email***************************************************/
                if ($payment->getStatus() == 'approved') {
                    $receipt_er = $this->getDoctrine()->getRepository(\App\Entity\Receipt::class);
                    $receipt = new Receipt(); 
                    /*Creacion Numeracion Factura (mejorar)*/
                    // dump($receipt_er->findLastId()->getId()); die;
                    $receipt->setNumberReceipt(
                        $receipt->generateNumberReceipt(
                            $receipt_er->findLastId()
                        )
                    );
                    $receipt->setFilePdfReceipt($receipt->nameFilePDF());

                    $response = $this->render('pdf/receipt.html.twig',
                                    [
                                        'payment' => $payment,
                                        'user' => $this->getUser(),
                                        'receipt' => $receipt,
                                    ]);
                    /*Creacion de PDF y obtencion de string de archivo comprimido*/
                    $outputPdf = $receipt->createReceiptPdf($response);
                    /*Creacion y escritura de archivo con string de PDF*/
                    $receipt->storeReceiptPdf($outputPdf, $receipt->getFilePdfReceipt());
                    $receipt->setUser($this->getUser());
                    $em->persist($receipt);
                    $em->flush();

                     /*Envio de Pago y Factura por Email*/
                    $payment->sendEmailPayment(
                        $this->mailer, 
                        $this->getUser(), 
                        $payment,
                        $receipt->getFilePdfReceipt(),
                    );
                    /*Envio de Pago y Factura por Email*/

                }
                
                $order->createOrder($data_order);
                $order->setReceipt($receipt);
                $em->persist($order);
                $em->flush();
                /*************************************************Creacion Factura*********************************************************/

                /*Crea detalle orden-producto y descuenta(en caso de pago aprobado) stock del producto*/
                $this->createOrderHasProducts($order, $payment);              
                return $this->render('payment/index.html.twig',
                    [
                        'payment' => $payment,
                        'user' => $this->getUser(),
                    ]);
            }
            
        }

        /*Ingresa al intentar ver el Pago*/
        if (null != $payment_id) {
            return $this->showPayment($payment_id);
        }

        return $this->redirectToRoute('app_my_orders');

    }


    /*Consulta de pago registrado en base de datos*/
    public function exist($id)
    {
        $repository = $this->getDoctrine()->getRepository(Payment::class);
        return $id != null ? $repository->find($id) : null;
    }
    

    public function showPayment($payment_id)
    {
        if ($this->exist($payment_id)) {
            $payment = new Payment($payment_id);
            return $this->render('payment/index.html.twig',
                [
                    'payment' => $payment,
                    'user' => $this->getUser(),
                ]);    
        }

        throw $this->createNotFoundException('The payment doesnt exist');
        
    }

    public function createOrderHasProducts(Order $order, Payment $payment)
    {
        if ($payment->getStatus() == 'approved' || $payment->getStatus() == 'in_process') {
            $cart = new Cart();
            if (!$cart->total_items()) {
                return;
            }
            $em = $this->getDoctrine()->getManager();
            foreach ($cart->contents() as $key) {
                $orderHasProduct = new OrderHasProducts();
                $data_order_products = 
                [
                    'product' => $this->getDoctrine()->getRepository(\App\Entity\Product::class)->find($key['id']),
                    'order' => $order,
                    'qty'  => $key['qty'],
                    /*En caso de pago no aprobado y estado in_process se reservan los productos*/
                    'reserved' => $payment->getStatus() == 'in_process'  ? '1' : null,
                ];
                /*Setea datos de los registros*/
                $orderHasProduct->createOrderHasProducts($data_order_products);
                /*Reliza el descuento de stock solo si pago sta aprobado*/
                if ($payment->getStatus() == 'approved') {
                  $orderHasProduct->getProducts()->getInventory()->decrementQty($orderHasProduct->getQuantity());
              }
              $em->persist($orderHasProduct);
              $em->flush();
          }
          $cart->destroy();
          return true; 
      }

      return [
          'status' => $payment->getStatus(),
          'status_detail' => $payment->getStatusDetail(),
      ];   

  }


}
