<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use MP\MP;
use App\Entity\Payment;
use App\Entity\Order;
use App\Entity\OrderHasProducts;
use Cart\Cart;

class MPController extends AbstractController
{


    /**
     * URL utilizada por API MP
     * @Route("/process_payment", name="app_process_payment_mp")
     */
    public function process_payment(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $cart = new Cart();
        $mp = new MP(); 
        $arrayContent = [];
        if (!$request->getContent()) {
            return $arrayContent[] = null;
        }

        /*Body response json*/
        /*Tratamiento de pago con SDK de MP*/
        $arrayContent = json_decode($request->getContent(), true);

        /*Guarda y devuele el estado del pago*/
        $response = $mp->payment($arrayContent);

        /*Verificar status y status_detail*/

        /*Se guardan detalles del Pago en base de datos*/
        $payment = new Payment();
        $payment->createPayment($response);
        /*Asignacion de id_payer MP  --> verificar si existe*/
        $this->getUser()->setPayerMPId($response['payer_id']);
        $em->persist($payment);
        $em->flush();
        
        /*Creacion  de Orden*/
        $order = new Order();
        $order_date = 
        [
            'user' => $this->getUser(),
            'payment' => $payment,
            'status' => 'pending',
            'shipping_address' => 'shipping_address_change',
        ]; 

        $order->createOrder($order_date);
        $em->persist($order);
        $em->flush();

        
        /*Creacion Detalle de Order con Productos*/
        $this->createOrderHasProducts($order);
        if ($response['status'] == 'in_process' || $response['status'] == 'approved') {
          $cart->destroy();
        }

        /**
         * Response payment json
         */
        $response_json = new Response();
        $response_json->setContent(json_encode($response));
        $response_json->headers->set('Content-Type', 'application/json');
        return $response_json;

    }



    // public function createOrderHasProducts(Order $order)
    // {
    //     $payment_search = $order->getPayment()->searchPayment($order->getPayment()->getPaymentId());
    //     // dump(json_decode($payment_search));
    //     $payment_details = json_decode($payment_search);
    //     // dump($payment_details);
    //     if ($payment_details->status == 'approved' || $payment_details->status == 'in_process') {
    //         $cart = new Cart();
    //         if (!$cart->total_items()) {
    //             return;
    //         }
    //         $em = $this->getDoctrine()->getManager();
    //         foreach ($cart->contents() as $key) {
    //             $orderHasProduct = new OrderHasProducts();
    //             $data_order_products = 
    //                 [
    //                     'product' => $this->getDoctrine()->getRepository(\App\Entity\Product::class)->find($key['id']),
    //                     'order' => $order,
    //                     'qty'  => $key['qty'],
    //                     'reserved' => $payment_details->status == 'in_process'  ? '1' : null,
    //                 ];
    //             $orderHasProduct->createOrderHasProducts($data_order_products);
    //             /*Reliza el descuento de stock*/
    //             if ($payment_details->status == 'approved') {
    //               $orderHasProduct->getProducts()->getInventory()->decrementQty($orderHasProduct->getQuantity());
    //             }
    //             $em->persist($orderHasProduct);
    //             $em->flush();
    //         }
    //         return true; 
    //     }

    //     return [
    //               'status' => $payment_details->status,
    //               'status_detail' => $payment_details->status_details,
    //            ];   
        
    // }



    /**
     * Utilizado por checkout pro
     * @Route("/create_preference", name="app_create_preference_mp")
     */
    public function create_preference(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $billing_address_form = json_decode($request->getContent(), true);
        $cart = new Cart();
        if (!$cart->total_items()) {
            return;
        }
        \MercadoPago\SDK::setAccessToken($_ENV['MP_ACCESS_TOKEN']);
        $preference = new \MercadoPago\Preference();
        $items = array();
        foreach ($cart->contents() as $key) {
            $item = new \MercadoPago\Item();
            $item->id = $key['id'];
            $item->title = $key['name'];
            $item->currency_id = "ARS";
            /*Crear la URL*/
            $item->picture_url = $key['images'][0];
            $item->description = $key['description'];
            /*Verificar categoria*/
            // $item->category_id = $key['description'];
            $item->quantity = $key['qty'];
            $item->unit_price = $key['saleprice'];
            array_push($items,$item);
            
        }

        $preference->items = $items;
        $preference->additional_info = "Productos Tienda E!";
        $preference->statement_descriptor = "Tienda E!";
        $preference->back_urls = [
            /*Ingresando por esta url , ingresa al PaymentController::index* el cual setea los valores en BD */
            "success" => "tienda_e.test/payment",
            "failure" => "tienda_e.test/check/out",
            /*Ingresando por esta url , ingresa al PaymentController::index* el cual setea los valores en BD */
            "pending" => "tienda_e.test/payment",
        ];
        $preference->auto_return = "approved";
        $preference->external_reference = "TEST-MP2021";
        $payer = new \MercadoPago\Payer();
        $payer->name = $billing_address_form['person_check_out_name'];
        $payer->surname = $billing_address_form['person_check_out_lastName'];
        $payer->email = $billing_address_form['person_check_out_email'];
        $payer->address = 
            [
                'zip_code' => $billing_address_form['person_check_out_zip_code'],
                'street_name' => $billing_address_form['person_check_out_address'],
                'street_number' => $billing_address_form['person_check_out_address_number'],
            ];
        $payer->phone = 
            [
                'area_code' => $billing_address_form['person_check_out_phone_area_code'],
                'number' => $billing_address_form['person_check_out_phone_number'],
            ];
        $payer->email = $billing_address_form['person_check_out_email'];
        $payer->identification = 
            [
                'type' => 'DNI', 
                'number' => $billing_address_form['person_check_out_dni'],
            ];

        $preference->payer = $payer;

        $preference->save();

        $preference = [
            'id' => $preference->id,
            'collector_id' => $preference->collector_id,
        ];
        $response = new Response();
        $response->setContent(json_encode($preference));
        $response->headers->set('Content-Type', 'application/json');
        return $response;  
    }




}
