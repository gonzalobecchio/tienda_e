<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\UserEditPanelFormType;
use App\Form\ProductFormType;
use App\Form\SearchUserFormType;
use App\Form\SearchProductFormType;
use App\Form\SearchOrderFormType;
use App\Entity\User;
use App\Entity\Product;
use App\Entity\Category;
use App\Entity\Inventory;
use App\Entity\Order;
use App\Entity\Receipt;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;


use Symfony\Component\HttpFoundation\ResponseHeaderBag;


/**
 * Require ROLE_SUPER_ADMIN for *every* controller method in this class.
 * @isGranted("ROLE_SUPER_ADMIN")
 */
class PanelAdminController extends AbstractController
{
    
    protected $images = [];

    function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }   

    /**
     * @Route("/panel/admin", name="app_panel_admin")
     */
    public function index(): Response
    {
        return $this->render('panel_admin/base.html.twig');
    }

    /**
     * @Route(
            "/panel/admin/users/{page}/{search}", 
             name="app_panel_admin_users",
             requirements = {"page" = "\d+"}
             )
     */
    public function panel_user(Request $request, $page = 1, $search = null)
    {
        $this->denyAccessUnlessGranted("ROLE_SUPER_ADMIN");
        $user = new User();
        $form = $this->createForm(SearchUserFormType::class, $user);
        $form->handleRequest($request);
        /*Busca todos los usuarios menos el user log y super_admin*/
        $users = $this->getDoctrine()
                      ->getRepository(User::class)
                      ->findAllNeqUserLoged($this->getUser(), 
                                            $page, 
                                            $search ? true : false, 
                                            $search ? $search : null
                        );

        /*cuando realiza el click para ejecutar la busqueda*/
        if ($form->get('save')->isClicked()) {
            $search =  $request->request->get('search_user_form')['email']; 
            // dump($search);
            $users = $users = $this->getDoctrine()
                                   ->getRepository(User::class)
                                   ->findAllNeqUserLoged($this->getUser(), 1, true, trim($search));
                                                   
           /*Alert informacin busqueda*/
            if ($users['pagination_object']->getTotalRegister() == 0) {
                $this->addFlash('danger', "No found results for '" . strtoupper($search)  . "'");
                $users = $this->getDoctrine()
                              ->getRepository(User::class)
                              ->findAllNeqUserLoged($this->getUser());
            }else{
                empty($search) ?  $this->addFlash('success', $users['pagination_object']->getTotalRegister() . ' Results found')
                               :  $this->addFlash('success', $users['pagination_object']->getTotalRegister() . ' Results found for ' . strtoupper($search));
            }
            /*Alert informacin busqueda*/
        }
        ###Pintando las coincidencias buscadas###   
        foreach ($users['paginator'] as $user) {
            $user->setEmail(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $user->getEmail()));
        }
        ###Pintando las coincidencias buscadas###
        return $this->render('panel_admin/users.panel-admin.html.twig',
            [
                'users' => $users,
                'search'=> $search
            ]);
    }

     /**
      * @Route("/panel/admin/edit/{id}/{page}/{search}", 
                name="app_panel_admin_edit_user", 
                requirements={"id"="\d+"}
            ) 
      */
     public function editUserRole(Request $request , int $id, int $page = 1, $search = null)
     {
         $this->denyAccessUnlessGranted("ROLE_SUPER_ADMIN");
         $em = $this->getDoctrine()->getManager();
         $er = $this->getDoctrine()->getRepository(User::class);
         $user = $er->find($id);
         $before_privilegy = $user->getRoles()[0];
         // dump($user->getRoles()[0]);
         $options = [
            'arrayRoles' => $user->selectedRole($user->getRoles()[0]), 
         ];
         $form = $this->createForm(UserEditPanelFormType::class, $user, $options);
         $form->handleRequest($request);
         if ($form->isSubmitted() && $form->isValid()) {
             $user = $form->getData();
             /*Utilizado para cambiar la password o guardar*/
             // $user->setPassword($this->passwordEncoder->encodePassword($user, $user->getPassword()));
             $em->flush();
             $this->addFlash('success',  $user->getEmail() . " " . 'Privileges were changed from ' . $before_privilegy  . ' to ' . $user->getRoles()[0]);
             return $this->render('panel_admin/users.panel-admin.html.twig', 
                [
                    'users' => $er->findAllNeqUserLoged($this->getUser(), $page),
                    'search' => $search,
                ]);
         }

         return $this->render('panel_admin/user.panel-admin.form.html.twig',
            [
                'form' => $form->createView(),
            ]);

     }

     /*Funcion de renderizado buscador usuarios*/
     /*Funcion de renderizado buscador prodctos*/
     public function renderSearch($section = null)
     {
         if ($section == 'panel_user') {
             $user = new User();
             $form = $this->createForm(SearchUserFormType::class, $user);
             return $this->render('_fragments/_form_search_users.html.twig',
                [
                    'form_search_user' => $form->createView(),
                ]);
         }

         if ($section == 'panel_products') {
             $product = new Product();
             $form = $this->createForm(SearchProductFormType::class, $product);
             return $this->render('_fragments/_form_search_products.html.twig',
                [
                    'form_search_product' => $form->createView(),
                ]);
         }

         if ($section == 'panel_orders') {
             $product = new Product();
             $form = $this->createForm(SearchOrderFormType::class, $product);
             return $this->render('_fragments/_form_search_orders.html.twig',
                [
                    'form_search_orders' => $form->createView(),
                ]);
         }
     }


     /**
      * @Route("/panel/admin/products/{page}/{search}",
               name="app_panel_admin_products",
               requirements={"page"="\d+"}
      * ) 
      */
     public function panel_products(Request $request, $page = 1, $search = null)
     {
         $this->denyAccessUnlessGranted("ROLE_SUPER_ADMIN");
         /*All Products Paginator*/
         $products = $this->getDoctrine()->getRepository(Product::class)->findAllProductsPaginator($page, false, $search);
         $product = new Product();
         $form = $this->createForm(SearchProductFormType::class, $product);
         $form->handleRequest($request);
         if ($form->get('save')->isClicked()) {
            /*Product Paginator with Where multiple*/
            $search =  $request->request->get('search_product_form')['search_product']; 
            $products = $products = $this->getDoctrine()
                                         ->getRepository(Product::class)
                                         ->findAllProductsPaginator(1, false, empty($search) || $search === null ? null : $search);
            /*Alert informacin busqueda*/
            if ($products['pagination_object']->getTotalRegister() == 0) {
                $this->addFlash('danger', "No found results for '" . strtoupper($search) . "'");
                $products = $this->getDoctrine()
                                ->getRepository(Product::class)
                                ->findAllProductsPaginator(1);
            }else{
                empty($search) ?  $this->addFlash('success', $products['pagination_object']->getTotalRegister() . ' Results found')
                               :  $this->addFlash('success', $products['pagination_object']->getTotalRegister() . ' Results found for ' . strtoupper($search));
            }
            /*Alert informacin busqueda*/
         }
            /*Pintando las coincidencias encontradas*/
            foreach ($products['paginator'] as $product) {
                $product->setReference(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $product->getReference()));
                $product->setName(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $product->getName()));
                $product->setIdString(strval($product->getId()));
                $product->setIdString(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $product->getIdString()));  
            }
            /*Pintando las coincidencias buscadas*/

         return $this->render('panel_admin/products.panel-admin.html.twig', 
            [
                'products' => $products,
                'search' => $search,
            ]);
     }


     /**
      * @Route("/panel/admin/product/{id}",
      *         name="app_panel_admin_product",
      *         requirements={"page"="\d+"})
      */
     public function form_product(Request $request , int $id = null)
     {
        /*Obtenemos el producto o creamos sino*/
        $product = $id === null ? new Product()  
                                : $this->getDoctrine()
                                       ->getRepository(Product::class)
                                       ->find($id);

        /*Creado para persistir el objeto*/
        $er_product = $id === null ? $this->getDoctrine()
                                          ->getRepository(Product::class) 
                                   : null;              
        /*Creacion de FormType Inventary con/sin datos */
        if ($product->getInventoryCollection() === null) {
            $array_collection = new ArrayCollection();
            $array_collection->add($product->getInventory());
            $product->setInventoryCollection($array_collection);
        }else{
            $inventory = $product->getInventory() != null ? $product->getInventory() 
                                                          : new Inventory();                                        
            $product->getInventoryCollection()->add($inventory);
            $product->setInventory($inventory);
        }
        
        if ($request->isXmlHttpRequest()) {
            dump($product);
        }
        $form = $this->createForm(ProductFormType::class, $product);
        $form->handleRequest($request);
        $em = $this->getDoctrine()->getManager();
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                 /*Tratamiento imagen*/ 
                if (count($form->get('images')->getData())) {
                    /*SI SE CARGAN IMAGENES INGRESA SINO CARGA POR default.png*/
                    $imagesFiles = $form->get('images')->getData();
                    if (is_array($imagesFiles) && count($imagesFiles)) {
                        foreach ($imagesFiles as $key => $value) {
                            $originalName =  pathinfo($value->getClientOriginalName(), PATHINFO_FILENAME);
                            $newFileName = 'uploads/img/' . md5($originalName).'.'.$value->guessExtension();
                            $this->images[] = $newFileName; 
                             try {
                                    $value->move(
                                        $this->getParameter('images_dir'),
                                        $newFileName
                                    );
                                } catch (FileException $e) {
                                    dump($e);
                                }
                        }
                    } 
                } else {
                    if ($id) {
                         /*CUANDO NO SE CARGAN IMAGENES SETEAMOS CON LAS DE BD*/
                         foreach ($product->getImages() as $key => $value) { 
                            $this->images[] = $value;
                         }
                    }else{
                        /*CUANDO NO SE CARGAN IMAGENES SETEAMOS POR DEFECTO*/
                        $this->images[] = 'default.png';
                    }
                }
                /*Tratamiento imagen*/ 
                $product->setImages($this->images);
                if ($er_product !== null) {
                    $lastID = $er_product->findLastID()->getId() + 1;
                    $product->createReference(
                        $product->getCategory()->getParent()->getName(), 
                        $lastID);
                }
                $product = $form->getData();
                $product->setInventory($product->getInventoryCollection()[0]);
                $em->persist($product);
                $em->flush();

                $id ? $this->addFlash('warning','Successfully Edited Product!') : $this->addFlash('success','Product Added Successfully!');
                return $this->redirectToRoute('app_panel_admin_products');
            }
        }
        return $this->render('panel_admin/product.panel-admin.html.twig',
            [
                'form_product' => $form->createView(),
                'title_header' => 'form product',
            ]);
     }


     public function dashboard()
     {  
         $today = new \DateTime();
         $receipt = $this->getDoctrine()->getRepository(\App\Entity\Receipt::class);
         $payment = $this->getDoctrine()->getRepository(\App\Entity\Payment::class);
         $product = $this->getDoctrine()->getRepository(\App\Entity\Product::class);
         $orderHasProduct = $this->getDoctrine()->getRepository(\App\Entity\OrderHasProducts::class);
         // dump($orderHasProduct->ordersTopSelling($today)); die;
         return $this->render('panel_admin/dashboard.html.twig',
            [
                'today'                   => $today->format('d-m-Y'),
                'qty_sales'               => $receipt->totalReceipt($today),
                'total_sales'             => round($payment->sumAllPaymentsToday($today), 2),
                'avg_ticket'              => round($payment->averageTicket($today), 2),
                'total_inventory'         => round($product->totalPriceInventory(), 2),
                'products_critical_stock' => $product->productsWithCrticalStock(),
                'quantity_inventory'      => $product->sumTotalStock(),
                'ordersTopSelling'        => $orderHasProduct->ordersSelling($today, "ASC"),
                'ordersBottomSelling'     => $orderHasProduct->ordersSelling($today, "DESC")
            ]);
     }



     
     /**
      * @Route(path="/panel/admin/orders/{page}/{search}", 
      *        name="app_panel_admin_orders",
      *        requirements={"page"="\d+"}
      *)
      */
     public function panel_orders(Request $request, $page = 1, $search = null)
     {
         $orders_repo = $this->getDoctrine()->getRepository(\App\Entity\Order::class);
         $orders = $orders_repo->findAllOrdersPaginator(1, null, true);
         $order  = new Order();
         $form = $this->createForm(SearchOrderFormType::class, $order);
         $form->handleRequest($request);
         if ($form->get('save')->isClicked()) {
            $search =  trim($request->request->get('search_order_form')['search']);
            
            
            $orders = $orders_repo->findAllOrdersPaginator(1, $search, true);
            /*Alert informacin busqueda*/
            if ($orders['pagination_object']->getTotalRegister() == 0) {
                $this->addFlash('danger', "No found results for '" . strtoupper($search) . "'");
                $orders = $this->getDoctrine()
                               ->getRepository(\App\Entity\Order::class)
                               ->findAllOrdersPaginator(1, null, true);
            }else{
                empty($search) ?  $this->addFlash('success', $orders['pagination_object']->getTotalRegister() . ' Results found')
                               :  $this->addFlash('success', $orders['pagination_object']->getTotalRegister() . ' Results found for ' . strtoupper($search));
            }
            /*Alert informacin busqueda*/
         }
        ##Pintando coincidencias##
         foreach ($orders['paginator'] as $order) {
            $order->setPaymentPaintSearch(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $order->getPayment()->getId()));
            $order->setOrderId(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $order->getOrderId()));
            //Funciona pero , verificar cuando el usuario que se busca es el admin y esta logueado
            // $order->getUser()->setEmail(str_ireplace($search,'<span class="paint-coincidence">'.$search.'</span>', $order->getUser()->getEmail()));
        }
         ##Pintando coincidencias

         return $this->render('panel_admin/orders.panel-admin.html.twig',
            [
                'orders' => $orders,
                'search' => $search,
            ]);
     }



     /**
      * @Route("/receipt/{filename}",
      * name="receipt_load",
      *)
      */     
     public function copyPdftoPublicDir($filename)
     {
        $file_sys = new Filesystem();
        // dump('..' . $_ENV['SAFE_DIR'] . '/' . date('Y') . '/' . $filename); die;
        /*En caso de no existir el direcotrio temporal de descarga, lo crea*/
        if (!$file_sys->exists($_ENV['DOWNLOAD_DIR'])) {
            $file_sys->mkdir($_ENV['DOWNLOAD_DIR'], 0700);
            //Acceso total solo al dueño
            // echo 'Directorio creado con exito!';
        }else{
            /*Eliminacion de todos los directorios temporales anteriores para evitar sobre cargas de espacio en el servidor*/
            /***Modificar en caso de dos o mas usuarios administradores*/
            if ($dir = opendir($_ENV['DOWNLOAD_DIR'])) {
                while (false !== $entrada = readdir($dir)) {
                    if ($entrada !== '.' && $entrada !== '..') {
                        /*Eliminacion del archivo para poder eliminar el directorio*/
                        $fileDelete = scandir($_ENV['DOWNLOAD_DIR'].'/' . $entrada)[2];
                         unlink($_ENV['DOWNLOAD_DIR'] . '/' .$entrada . '/'. $fileDelete);
                         rmdir($_ENV['DOWNLOAD_DIR'] . '/'. $entrada);
                    }
                }
            }

        }

        /*Verifica la existencia del archivo en la carpeta de acceso restrigido*/
        if (!$file_sys->exists('..' . $_ENV['SAFE_DIR'] . '/' . date('Y') . '/' . $filename)) {
            exit('File not found');
        }

        /*Creacion nombre aleatorio de directorio temporal*/
        $randomDirName = uniqid();

        /*Creacion de Directorio Temporal*/
        $file_sys->mkdir($_ENV['DOWNLOAD_DIR'] . '/' . $randomDirName);
        
        /*Copia del Archivo desde la carpeta sin acceso publico a la carpeta publica*/
        $file_sys->copy('..' . $_ENV['SAFE_DIR'] . '/' . date('Y') . '/' . $filename, $_ENV['DOWNLOAD_DIR'] . '/' . $randomDirName . '/'. $filename);

        /*Ubicacion del archivo para descargar sobre el template*/
        $pathDowloadFile = $_ENV['DOWNLOAD_DIR'] . '/' . $randomDirName . '/'. $filename;

         return $this->render('panel_admin/iframe_receipt.html.twig',
            [
                'file_dir' => $pathDowloadFile,
            ]);
     }


}
