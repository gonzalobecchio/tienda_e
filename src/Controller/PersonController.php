<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\PersonFormType;
use App\Entity\Person;

class PersonController extends AbstractController
{
    /**
     * @Route("/person/upd/my/dates", name="app_person_my_dates")
     */
    public function index(Request $request): Response
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        if (null == $this->getUser()) {
            return new Response('User not logued');
        }

        $person = $this->getUser()->getPerson();
        $form = $this->createForm(PersonFormType::class, $person);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $person = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($person);
            $em->flush();
            $this->addFlash('notice', 'Your changes were saved!');
            return $this->redirectToRoute('app_person_my_dates');
        }
        return $this->render('person/index.html.twig', [
            'form_person' => $form->createView(),
        ]);
    }
}
