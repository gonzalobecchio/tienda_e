<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StoreController extends AbstractController
{
    /**
     * @Route("/store", name="app_store")
     */
    public function index(): Response
    {
        return $this->render('store/index.store.html.twig');
    }

    public function loadBreadcumStore($category_parent_data, $array_names_categorys = null)
    {
        return $this->render('_fragments/_breadcum.store.html.twig', 
            [
                'category_parent_data' => $category_parent_data,
                'array_names_categorys' => $array_names_categorys,
            ]);
    }
}
