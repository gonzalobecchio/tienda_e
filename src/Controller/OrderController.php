<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Order;

class OrderController extends AbstractController
{
    /**
     * @Route("/orders/{page}/{search}",
     * name="app_my_orders",
     * requirements={"page"="\d+"}
     *)
     */
    public function index(Request $request, $page = 1, $search = null): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER'); 
        $em = $this->getDoctrine()->getRepository(Order::class);
        $orders = $em->findAllOrdersPaginator(1, null, false, $this->getUser());
        return $this->render('order/index.html.twig', 
            [
                'orders' => $orders,
                'search' => $search,
            ]);
    }
}
