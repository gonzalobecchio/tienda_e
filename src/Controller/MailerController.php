<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use App\Entity\Receipt;

/*Biblioteca creacion PDF*/
use Dompdf\Dompdf;
use Dompdf\Options;

class MailerController extends AbstractController
{
    /**
     * @Route("/factura", name="factura")
     */
    public function index(Request $request): Response
    {

        // $receipt_er = $this->getDoctrine()->getRepository(\App\Entity\Receipt::class);
        // dump($receipt_er->findLastId()); die;
        $receipt = new Receipt();
        // $receipt->storeReceiptPdf(); die;

       $options  = new Options();
       /*Habilitacion acceso remoto a archivo locales o en otros servidores*/
       $options->set('isRemoteEnabled', true);
       $options->set('chroot', '/');
       $dompdf = new Dompdf($options);

        $producto_1 = 
            [
                'title' => 'Este es el producto 1' ,
                'quantity' => 25,
                'unit_price' => 144.2
            ];
        $producto_2 = 
            [
                'title' => 'Este es el producto 2' ,
                'quantity' => 60 ,
                'unit_price' => 277.9
            ];
        $producto_3 = 
            [
                'title' => 'Este es el producto 3' ,
                'quantity' => 19,
                'unit_price' => 1870.3
            ];
        $producto_4 = 
            [
                'title' => 'Este es el producto 4' ,
                'quantity' => 8,
                'unit_price' => 58.3
            ];
        $producto_5 = 
            [
                'title' => 'Este es el producto 5' ,
                'quantity' => 1,
                'unit_price' => 120.3
            ];
        $producto_6 = 
            [
                'title' => 'Este es el producto 6' ,
                'quantity' => 14,
                'unit_price' => 1200.3
            ];

        $productos = [];
        array_push($productos,$producto_1, $producto_2, $producto_3,$producto_4, $producto_5, $producto_6);
        $response = $this->render('pdf/receipt.html.twig',
            [
                'data_payment' => $productos,
            ]);
        $html = $response->getContent();
        $dompdf->loadHtml($html);
        $dompdf->setPaper('letter', 'portrait');
        $dompdf->render();
        $output = $dompdf->output();
        // $dompdf->stream();
        $file = $output.'.pdf';
        
        $receipt->storeReceiptPdf($file,'name');
        return new Response('PDF OK');
       
    }

    /**
     * @Route("/send", name="send_mailer")
     */
    public function sendEmail(MailerInterface $mailer)
    {
        $email = (new TemplatedEmail())
            ->from('gonzalobecchio@gmail.com')
            ->to('gonzalobecchio@gmail.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->htmlTemplate('mailer/index.html.twig')
            ;

        $mailer->send($email);
        // return new Response('Email Send Success!!');
    }
}
