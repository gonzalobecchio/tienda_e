<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Spipu\Html2Pdf\Html2Pdf;

class IndexController extends AbstractController
{
    /**
     * @Route("/{page}", name="index", requirements={"page"="\d+"})
     */
    public function index(int $page = 1): Response
    {
        $repository = $this->getDoctrine()->getRepository(\App\Entity\Product::class);
        $products_paginator = $repository->findAllProductsPaginator($page, true);
        // dump($products_paginator);
        return $this->render('index/index.html.twig', [
            'title' => 'Tienda E!',
            'products' => $products_paginator['paginator'],
            'count_pages' => $products_paginator['pagination_object']->getCountPages(),
            'total_register' => $products_paginator['pagination_object']->getTotalRegister(),
            'page' => $products_paginator['pagination_object']->getPage(),
            'ITEMBYPAGE' => $products_paginator['ITEMBYPAGE'],
        ]);
    }
}
