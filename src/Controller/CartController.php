<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Cart\Cart;

class CartController extends AbstractController
{
    /**
     * @Route("/cart/all-products", name="cart")
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        $cart = new Cart();
        if ($cart->total_items() != 0) {
            return $this->render('cart/index.cart.html.twig', [
                'cart' => $cart,
            ]);
        }

        return $this->render('cart/index.cart.html.twig');
        
    }
}
