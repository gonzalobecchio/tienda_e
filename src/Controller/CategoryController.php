<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends AbstractController
{
    /**
     * @Route("/category", name="category")
     */
    public function index(): Response
    {
        return $this->render('category/index.html.twig', [
            'controller_name' => 'CategoryController',
        ]);
    }

    /**
      * @Route("/categorys/all", name="category_all")  
      */
    public function allCategorys($_fragment)
    {
        $repository = $this->getDoctrine()->getRepository(\App\Entity\Category::class);
        $categorys = $repository->findCategoryParents();   
        return $this->render($_fragment, 
            [
                'categorys' => $categorys,
            ]
        );
    }
}
