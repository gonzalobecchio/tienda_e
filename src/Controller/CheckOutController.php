<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Cart\Cart;
use App\Form\PersonCheckOutType;
use App\Entity\Person;

class CheckOutController extends AbstractController
{
    protected $cart;

    function __construct()
    {
        $this->cart = new Cart();
    }

    /**
     * @Route("/check/out", name="app_check_out")
     */
    public function index(): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER');
        if ($this->cart->total_items() != 0) {
         $person = new Person(); 
         $user = $this->getUser();
         $person = $user->getPerson();
         $form = $this->createForm(PersonCheckOutType::class, $person, 
            [
                'user' => $this->getUser(),
            ]);
         return $this->render('check_out/index.html.twig', [
           'cart' => $this->cart,
           'form' => $form->createView(),
       ]);
     }

     return $this->render('cart/empty_cart.html.twig');

 }
}
