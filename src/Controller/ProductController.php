<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Cart\Cart;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends AbstractController
{
    

    protected $session;

    function __construct()
    {
        $this->session = new Session();
    }

    /**
     * @Route("/product", name="product")
     */
    public function index(): Response
    {
        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }


    /**
     * @Route("/product/{id}", name="app_product_show", requirements={"id"="\d+"});
        */
    public function showProduct($id)
    {
        $repository = $this->getDoctrine()->getRepository(\App\Entity\Product::class);
        $product = $repository->find($id);
        if (!$product || $product->getStatus() == 'inactive') {
             throw $this->createNotFoundException('The product does not exist');
        }
        return $this->render('product/product.html.twig', 
            [
                'product' => $product,
            ])
        ;

    }

    /**
     * @Route("product/category/{page}/{id}", name="app_product_by_category", requirements={"id"="\d+", "page"="\d+"});
     */
    public function showProductsByCategory(Request $request, $page = 1, $id = null)
    {
        /*Obtiene las categorias desde el menu aside*/
        $ids_categorys_filter = $request->get('arr_categorys') ? $request->get('arr_categorys'): null;

        if ($request->headers->get('x-section')) {
            /*Verifica si se selecciono una pagina del paginado desde ajax*/
            $page = $request->get('page_jqXHR') ? $request->get('page_jqXHR') : $page;
        }

        $repository = $this->getDoctrine()->getRepository(\App\Entity\Product::class);

        /*Nombres de Categorias seleccionados*/
        $array_names_categorys = $ids_categorys_filter != null ? $this->getDoctrine()
                                                                      ->getRepository(\App\Entity\Category::class)
                                                                      ->findNameCategoriesSelected($ids_categorys_filter)
                                                               : null;


        if ($ids_categorys_filter == null) {
            /*ID por parametro, representa la parent_id por la cual filtrar los productos*/
            $products_paginator = null != $id 
            ? $repository->findProductsByCategoryPaginator($id, $page)/*Esta linea filtra por id de categoria*/
            : $repository->findAllProductsPaginator($page, true); /*Pagina todos los productos*/   
        }else{
            $products_paginator = null != $ids_categorys_filter
            ? $repository->findProductsByCategoryPaginator($id, $page, $ids_categorys_filter) /*Filtra por los id de categorias aside*/
            : $repository->findAllProductsPaginator($page, true);
        }

        /*Datos sobre category parent*/
        $category_parent_data = $repository->findCategoryAndCant($id, $ids_categorys_filter);
        // dump($category_parent_data);

        /*Cantidad de productos por categoria*/
        $productsByCategory = $repository->findCantProductByParent();

        /*Filtro para saber si pertenece a una categoria o mostramos todos los productos*/
        $category_id = $id != null ? $id : null;

        /*Solo ingresar si no viene de filtros side*/
        // if ($ids_categorys_filter == null && $request->get('aside') == null) {
        if ($ids_categorys_filter == null && $request->headers->get('x-section') == null) {
            return $this->render('store/index.store.html.twig', 
                [
                    'title' => 'Store to Electro',
                    'products' => $products_paginator['paginator'],
                    'count_pages' => $products_paginator['pagination_object']->getCountPages(),
                    'total_register' => $products_paginator['pagination_object']->getTotalRegister(),
                    'page' => $products_paginator['pagination_object']->getPage(),
                    'ITEMBYPAGE' => $products_paginator['ITEMBYPAGE'],
                    'productsByCategory' => $productsByCategory,
                    'category_parent_data' => $category_parent_data,
                    'category_id' => $category_id,
                ]
            );
        }else{
            /*xsection header que me indica de donde proviene el filtro*/
            return $this->render('_fragments/_index_store_section.html.twig', 
                [
                    'title' => 'Store to Electro',
                    'products' => $products_paginator['paginator'],
                    'count_pages' => $products_paginator['pagination_object']->getCountPages(),
                    'total_register' => $products_paginator['pagination_object']->getTotalRegister(),
                    'page' => $products_paginator['pagination_object']->getPage(),
                    'ITEMBYPAGE' => $products_paginator['ITEMBYPAGE'],
                    'productsByCategory' => $productsByCategory,
                    'category_parent_data' => $category_parent_data,
                    'category_id' => $category_id,
                    'xsection' => $request->headers->get('x-section'),
                    // 'array_names_categorys' => $array_names_categorys,
                ]
            );
            
        }
    }


    /**
     * @Route("/product/cart/{id}", name="app_find_product_by_cart", requirements={"id"="\d+"}
     *)
     */
    public function addProductCart(int $id, Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');
        $cart = new Cart();
        $productOb = $this->getDoctrine()->getRepository(\App\Entity\Product::class)->find($id);
        $product = !$id ? null : $this->getDoctrine()
                                      ->getRepository(\App\Entity\Product::class)
                                      ->findProductAsArray($id);

        $salePrice = $productOb->getSales() && $productOb->getPrice() > 0 ? ['saleprice' => $productOb->getSalePrice()] : null;

        $qty = $request->get('qty') ? ['qty' => $request->get('qty')] : null;

        $item = array_merge($product[0], $qty, $salePrice);
        dump($item);

        $cart->addItem($item);
        $cart->save();
        return $this->cartIcon();

    }

    /**
     * @Route("/product/cart/delete/{item}" , name="app_delete_item_cart")
     */
    public function removeItem(Request $request)
    {
        $cart = new Cart();
        $item_key = $request->get('item');
        $rm = $cart->remove_item($item_key);
        if (!$cart->total_items()) {
            return $this->render('cart/empty_cart.html.twig');
            // return $this->render('_fragments/_cart.index.html.twig');
        }

        return $this->render('_fragments/_cart.index.html.twig', 
            [
                'cart' => $cart,
                'messages' => $this->session->getFlashBag()->get('deleted'),
            ]
        );

    }

    public function cartIcon()
    {
        $cart = new Cart();
        return $this->render('_fragments/_cart.html.twig', 
            [
                'cart' => $cart,
            ]
        );
    }

    public function updateBreadcumCart()
    {
        $cart = new Cart();
        return $this->render('_fragments/_breadcum.cart.html.twig',
            [
                'cart' => $cart,
            ]
        );
    }

    /**
     *@Route("/clear", name="clear_session")
    */
    public function clearsession()
    {
        $session = new Session();
        $session->clear();
        return new Response();
    }


      /**
     *@Route("/cart", name="cart_session")
    */
      public function versession()
      {
        $session = new Session();
        dump($session->get('cart_session'));die;
        
    }




}
