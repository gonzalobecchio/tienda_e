<?php

namespace App\Form;

use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PersonTypeCollectionsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, 
                [
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => 'Name',
                ]

            )
            ->add('lastName', null, 
                [
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => 'Last Name',
                ]
            )
            ->add('dni', null, 
                [
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => 'DNI',
                ]
            )
            ->add('address', null,[
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => 'Address',
            ])
            ->add('address_number', null, [
                    'attr' => 
                         [
                             'class' => 'input', 
                         ],
                         'label' => 'Number',
                ]
            )
            ->add('zip_code', null, [
                'attr' => 
                [
                    'class' => 'input',
                ],
                'label' => 'Zip Code',
            ])
            ->add('phone_area_code', null, 
                [
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => 'Phone Area Code',
                ]
            )
            ->add('phone_number', null, 
                [
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => 'Phone Number',
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
        ]);
    }
}
