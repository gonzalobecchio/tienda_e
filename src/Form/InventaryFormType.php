<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use App\Entity\Inventory;

class InventaryFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('unit', ChoiceType::class,
                [
                    'choices' => 
                        [
                            'piece' => 'piece',
                            'combo' => 'combo',
                        ],
                        'attr' => 
                        [
                            'class' => 'input-admin-select',
                        ],
                ])
            ->add('availableQuantity', null,
                [
                    'attr' => 
                        [
                            'class' => 'input-admin',
                        ],
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Inventory::class,
        ]);
    }
}
