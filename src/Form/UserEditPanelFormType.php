<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\CallbackTransformer;


class UserEditPanelFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $arrayRoles = $options['arrayRoles'];
        $builder
            ->add('email', EmailType::class,
                [
                    'attr' => [
                        'class' => 'input',
                    ],
                    'disabled' => true,
                ])
            ->add('password', TextType::class,[
                    'attr' => [
                        'class' => 'input',
                    ],
                    'disabled' => true,
                ])
            ->add('roles', ChoiceType::class, 
                [
                    'choices' => $arrayRoles,
                    'expanded' => true,
                    'multiple' => false,
                    'choice_attr' => 
                        [
                            'class' => 'input-radio'
                        ],
                ])
            ;
            $builder->get('roles')
                    ->addModelTransformer(new CallbackTransformer(
                        function($rolesAsArray){
                            dump($rolesAsArray);
                             return implode(', ', $rolesAsArray);
                        },
                        function($rolesAsString){
                             dump($rolesAsString);
                             return explode(', ', $rolesAsString);
                        }
                    ))
            ;

            $builder->add('save', SubmitType::class, 
                [
                    'attr' => [
                        'class' => 'primary-btn',
                    ],
                ])
            ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'arrayRoles' => 
                [
                    "ROLE_USER",
                ],
        ]);

        $resolver->setAllowedTypes('arrayRoles', 'array');
    }
}
