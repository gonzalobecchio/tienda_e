<?php

namespace App\Form;

use App\Entity\Person;
use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;


class PersonCheckOutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $options['user'];
        $builder
            ->add('name', TextType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'First Name',
                        'class' => 'input',
                        'readonly' => 'readonly',
                    ],
                    'label' => false,

                ])
            ->add('lastName', TextType::class,
                [
                    'attr' => 
                    [
                        'placeholder' => 'Last Name',
                        'class' => 'input',
                        'readonly' => 'readonly',
                    ],
                    'label' => false,
                ])
            ->add('dni', TextType::class,
                [
                    'attr' => 
                    [
                        'placeholder' => 'DNI',
                        'class' => 'input',
                        'readonly' => 'readonly',
                    ],
                    'label' => false,

                ])
            ->add('email', EntityType::class, 
                [
                    'class' => User::class,
                    'query_builder' => function(EntityRepository $er) use ($user){
                        return $er->findEmailUserLog($user);
                    },
                    'choice_label' => 'email',
                    'attr' => 
                    [
                        'class' => 'input',
                    ],
                    'label' => false,
                    'mapped' => false,

                ])
            ->add('address', TextType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'Address',
                        'class' => 'input',
                    ],
                    'label' => false,
                ])
            ->add('address_number', TextType::class,
                [
                    'attr' => 
                    [
                        'placeholder' => 'Number',
                        'class' => 'input',
                    ],
                    // 'mapped' => false,
                    'label' => false,
                ])
            ->add('city', TextType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'City',
                        'class' => 'input',
                    ],
                    'label' => false,
                    'mapped' => false, 
                ])
            ->add('zip_code', TextType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'ZIP Code',
                        'class' => 'input',
                    ],
                    'label' => false,
                    // 'mapped' => false,
                ])
            ->add('phone_area_code', TextType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'Phone Area Code',
                        'class' => 'input',
                    ],
                    'label' => false,
                ])
            ->add('phone_number', TextType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'Phone Number',
                        'class' => 'input',
                    ],
                    'label' => false,
                ])
            ->add('order_notes', TextareaType::class, 
                [
                    'attr' => 
                    [
                        'placeholder' => 'Order Notes',
                        'class' => 'input',
                    ],
                    'label' => false,
                    'mapped' => false,
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'user' => false,
        ]);

        $resolver->setAllowedTypes('user', User::class);
    }
}
