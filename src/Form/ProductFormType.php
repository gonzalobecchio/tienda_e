<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\All;
use App\Entity\Category;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;





class ProductFormType extends AbstractType
{
    
   
    public function buildForm(FormBuilderInterface $builder, array $options)
    {   
        $builder
            ->add('name', TextType::class, 
                [
                    'attr' => 
                        [
                            'class' => 'input-admin',
                        ],
                ])
            ->add('description', TextareaType::class,
                [
                    'attr' => 
                        [
                            'class' => 'input-admin',
                        ],
                ])
            ->add('status', ChoiceType::class,
                [
                    'choices' => 
                        [
                            'Active' => 'active',
                            'Inactive' => 'inactive',
                        ],
                    'expanded' => true,
                    'attr' => 
                        [
                            'class' => 'input-admin-radio',
                        ], 
                    'required' => true,
                ])     
            // ->add('reference', TextType::class, 
            //     [
            //         'attr' => 
            //             [
            //                 'class' => 'input-admin'
            //             ],
            //         'disabled' => true,
            //     ])     
            ->add('price', TextType::class,
                [
                    'attr' => 
                        [
                            'class' => 'input-admin',
                        ],
                ])     
            ->add('parent', EntityType::class,
                [
                    'class' => Category::class,
                    'query_builder' => function(EntityRepository $er){
                        return $er->findCategoryParents(true);
                    },
                    'choice_label' => 'name',
                    'mapped' => false,
                    'attr' => 
                        [
                            'class' => 'input-admin-select',
                        ],
                    'label' => 'Category Parent',
                    'placeholder' => '',
                ])   
            ->add('images', FileType::class,
                [
                    'label' => false,
                    'mapped' => false,
                    'required' => false,
                    'multiple' => true,
                    'constraints' => 
                        [
                            new All(
                                [
                                    'constraints' => 
                                        new File(
                                            [
                                                'maxSize' => '2M',
                                                'mimeTypes' => 
                                                [
                                                    'image/png',
                                                ],
                                                'mimeTypesMessage' => 'The image {{ name }} with mimeTypes {{ type }} is invalid . Allowed mime types are {{ types }}',
                                            ],
                                        )
                                ]
                            )
                            

                        ],
                ])     
            ->add('inventory_collection', CollectionType::class,
                [
                    'entry_type' => InventaryFormType::class,
                    'entry_options' => 
                        [
                            'label' => false
                        ],
                    'by_reference' => false,
                ])     
            ->add('sales', TextType::class,
                [
                    'attr' => 
                        [
                            'class' => 'input-admin',
                        ],
                    'empty_data' => 0,
                ])     
            ->add('isNew', ChoiceType::class, 
                [
                    'choices' => 
                        [
                            'new' => true,
                        ],
                    'expanded' => true,
                    'multiple' => true,
                    'label' => false,
                    'attr' => 
                        [
                            'class' => 'input-admin-checkbox',
                        ],
                ])     
            ->add('save', SubmitType::class, 
                [
                    'attr' => 
                        [
                            'class' => 'input-admin-btn btn btn-primary',
                        ],
                ])     
            ;

            $builder->addEventListener(
                FormEvents::POST_SET_DATA, 
                function(FormEvent $event){
                    $form = $event->getForm();
                    $data = $event->getData();
                    $parent = $data->getCategory() !== null ? $data->getCategory()->getParent()->getId(): null;
                    $form->get('parent')->setData($data->getCategory() !== null ? $data->getCategory()->getParent(): null);
                    $form->add('category', EntityType::class,
                        [
                            'class' => Category::class,
                            'query_builder' => function(EntityRepository $er) use ($parent){
                                return $er->findChildrenCategory($parent);
                            },
                            'choice_label' => 'name',
                            'label' => 'Category Children',
                            'attr' => 
                                 [
                                     'class' => 'input-admin-select',
                                 ],
                        ]);

            });

            $builder->get('parent')->addEventListener(
                FormEvents::POST_SUBMIT,
                function(FormEvent $event){
                    $form = $event->getForm();
                    $category = $event->getData();
                    $form->getParent()->add('category', EntityType::class, 
                        [
                            'class' => Category::class,
                            'query_builder' => function(EntityRepository $er) use ($category){
                                return $er->findChildrenCategory($category);
                            },
                            'choice_label' => 'name',
                            'attr' => 
                                 [
                                     'class' => 'input-admin-select',
                                 ],
                            'label' => 'Category Children',
                        ]);
                }
            );

            $builder->get('isNew')
                    ->addModelTransformer(
                        new CallbackTransformer(
                            function($arrayAsBoolean){
                                // dump($arrayAsBoolean);
                                if ($arrayAsBoolean == null) {
                                    return null;     
                                }
                                if ($arrayAsBoolean == true) {
                                    return [
                                                'new' => true,
                                           ];
                                }
                            }, 
                            function($valueAsArray){
                                // dump($valueAsArray);
                                if ($valueAsArray === null || empty($valueAsArray)) {
                                    return null;
                                }
                                if (is_array($valueAsArray) && array_key_exists('new', $valueAsArray)) {
                                    return true;
                                }
                                else{
                                     return $valueAsArray[0];
                                }

                                
                               
                            }
                        )
                    );

            // $builder->get('images')->addEventListener(
            //     FormEvents::PRE_SUBMIT,
            //     function(FormEvent $event){
            //         $images = $event->getForm()->getParent()->getData()->getImages();
            //         $form = $event->getForm()->getParent()->setData($images);
            //         dump($form->getData());
            //         dump($images);
            //     }
            // );

           
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
