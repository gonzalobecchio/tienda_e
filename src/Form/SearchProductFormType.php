<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search_product', TextType::class,
                [
                    'label' => 'SEARCH',
                    'mapped' => false,
                    'required' => false,
                    'attr' => 
                        [
                            'class' => 'input-text-search',
                            'placeholder' => 'Product Name or ID or REF', 
                        ],
                ])
            ->add('save', SubmitType::class, 
                [
                    'label' => 'SEARCH',
                    'attr' => 
                        [
                            'class' => 'input-btn-search btn btn-primary',
                        ],
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
