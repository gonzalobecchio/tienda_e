<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;


class SearchUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, 
                [
                    'attr' => 
                        [
                            'class' => 'input-text-search',
                            'placeholder' => 'example@mail.com', 
                        ],
                    'required' => false,
                    'mapped' => false,
                    'label' => 'SEARCH',
                ])
            ->add('save', SubmitType::class, 
                [
                    'attr' => 
                        [
                            'class' => 'input-btn-search btn btn-primary',
                        ],
                    'label' => 'SEARCH',
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
