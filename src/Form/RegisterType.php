<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Type;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;


class RegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('persons', CollectionType::class,
            [
                'entry_type' => PersonTypeCollectionsType::class,
                'entry_options' => 
                [
                    'label' => false,
                ],
                'by_reference' => false,
            ]
        )
        ->add('email', EmailType::class,
            [
                'attr' => 
                [
                    'class' => 'input',
                ],
                'label' => 'Email',
            ]
        )
        ->add('password', PasswordType::class, 
            [
                'attr' => 
                [
                    'class' => 'input',
                ],
                'label' => 'Password',  
            ],
        )
        ->add('save', SubmitType::class,
            [
                'attr' => 
                [
                    'class' => 'primary-btn order-submit',
                ],
                'label' => 'Register',
            ]
        );
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
