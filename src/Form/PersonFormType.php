<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PersonFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'Name',
                    ],
                    'label' => false,
                ])
            ->add('lastName', TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'LastName',
                    ],
                    'label' => false,
                ])
            ->add('dni',TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'DNI',
                    ],
                    'label' => false,
                ])
            ->add('phone_area_code',TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'Phone Area Code',
                    ],
                    'label' => false,
                ])
            ->add('phone_number',TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'Phone Number',
                    ],
                    'label' => false,
                ])
            ->add('address',TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'Address',
                    ],
                    'label' => false,
                ])
            ->add('address_number',TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'Address Number',
                    ],
                    'label' => false,
                ])
            ->add('zip_code',TextType::class, 
                [
                'attr' => 
                    [
                        'class' => 'input',
                        'placeholder' => 'Zip Code',
                    ],
                    'label' => false,
                ])
            ->add('save', SubmitType::class, 
                [
                'attr' => 
                    [
                        'class' => 'primary-btn',
                    ],
                ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
