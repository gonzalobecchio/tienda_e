<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Product;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Faker\Factory;
use App\DataFixtures\CategoryFixtures;
use App\DataFixtures\InventoryFixtures;

class ProductFixtures extends Fixture implements DependentFixtureInterface
{
    public const CANT_INVENTORY_PRODUCT = 200;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for ($i=1; $i <= self::CANT_INVENTORY_PRODUCT; $i++) { 
           $product = new Product();
           $product->setName($faker->sentence(2,true));
           $product->setDescription($faker->realText(400));
           $product->setStatus('active');
           $product->setReference($faker->sentence(1, false) .'-' . $faker->numberBetween(1, self::CANT_INVENTORY_PRODUCT));
           $product->setPrice($faker->randomFloat(2, 3000, 99999));
           $product->setSales($faker->numberBetween(30,50));
           $product->setIsNew($faker->boolean(50));

           switch (rand(0, CategoryFixtures::$cant_array_parents)) {
            case 0:
            $product->setCategory($this->getReference(CategoryFixtures::LAPTOP.'_'. rand(0,CategoryFixtures::$cant_laptop)));
            $product->setReference('LAP' .'-' . $faker->numberBetween(1, self::CANT_INVENTORY_PRODUCT));
            $product->setImages(
                [   
                    'product01.png',
                    'product03.png',
                    'product08.png',
                ]
            );        
            break;
            case 1:
            $product->setCategory($this->getReference(CategoryFixtures::SMARTPHONE.'_'. rand(0,CategoryFixtures::$cant_smartphone)));
            $product->setReference('SMA'.'-' . $faker->numberBetween(1, self::CANT_INVENTORY_PRODUCT));
            $product->setImages(
                [
                    'product07.png',
                ]
            );        
            break;
            case 2:
            $product->setCategory($this->getReference(CategoryFixtures::CAMERA.'_'. rand(0,CategoryFixtures::$cant_camera)));
            $product->setReference('CAM'.'-' . $faker->numberBetween(1, self::CANT_INVENTORY_PRODUCT));
            $product->setImages(
                [
                    'product09.png',
                ]
            );        
            break;
            case 3:
            $product->setCategory($this->getReference(CategoryFixtures::ACCESSORIE.'_'. rand(0,CategoryFixtures::$cant_accessorie)));
            $product->setReference('ACC'.'-' . $faker->numberBetween(1, self::CANT_INVENTORY_PRODUCT));
            $product->setImages(
                [
                    'product04.png',
                ]
            );        
            break;
            case 4:
            $product->setCategory($this->getReference(CategoryFixtures::HEADPHONE.'_'. rand(0,CategoryFixtures::$cant_headphone)));
            $product->setReference('HEA'.'-' . $faker->numberBetween(1, self::CANT_INVENTORY_PRODUCT));
            $product->setImages(
                [
                    'product02.png',
                ]
            );        
            break;

        }
         // $product->setCategory($this->getReference(CategoryFixtures::LAPTOP.'_'. 0));
        $product->setInventory($this->getReference(InventoryFixtures::INVENTORY_REFERENCE.'_'.$i));
        $manager->persist($product);
        $manager->flush();    
    }

}

public function getDependencies()
{
    return [
        \App\DataFixtures\InventoryFixtures::class,
        \App\DataFixtures\CategoryFixtures::class,
        // \App\DataFixtures\BrandFixtures::class,
    ];
}
}
