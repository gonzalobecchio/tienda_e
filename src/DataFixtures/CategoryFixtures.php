<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Category;

class CategoryFixtures extends Fixture
{
    public const LAPTOP = 'Laptops';
    public const SMARTPHONE = 'Smartphone';
    public const CAMERA = 'Cameras';
    public const ACCESSORIE = 'Accessories';
    public const HEADPHONE = 'Headphone';

    public static $cant_parent;
    public static $cant_laptop;
    public static $cant_smartphone;
    public static $cant_camera;
    public static $cant_accessorie;
    public static $cant_headphone;

    public static $cant_array_parents;




    private $array_categorys_parent = 
    [
        self::LAPTOP,
        self::SMARTPHONE,
        self::CAMERA,
        self::ACCESSORIE,
        self::HEADPHONE,
    ];

    private $sub_laptops = 
    [
        'Children Laptop',
        'Children Laptop 2',
        'Children Laptop 3',
        'Children Laptop 4',
    ];
    private $sub_smartphones = 
    [
        'Children Smartphones',
        'Children Smartphones 1',
    ];
    private $sub_cameras = 
    [
        'Sub Camera',
        'Sub Camera 2',
        'Sub Camera 3',
        'Sub Camera 4',
        'Sub Camera 5',
        'Sub Camera 6',
        'Sub Camera 7',
    ];
    private $sub_accessories = 
    [
       'Sub Category a',  
   ];
   private $sub_headphone = 
   [   
    'Sub Category Headphone',
    'Sub Category Headphone 2',
    'Sub Category Headphone 3',
];

protected $array_categorys;

function __construct()
{
    $this->array_categorys = array_merge(
        $this->array_categorys_parent,
        $this->sub_laptops,
        $this->sub_smartphones,
        $this->sub_cameras,
        $this->sub_accessories,
        $this->sub_headphone
    );
    // print_r($this->array_categorys);die;

    self::$cant_laptop = count($this->sub_laptops) - 1;
    self::$cant_smartphone = count($this->sub_smartphones) -1;
    self::$cant_camera = count($this->sub_cameras) - 1;
    self::$cant_accessorie = count($this->sub_accessories) - 1;
    self::$cant_headphone = count($this->sub_headphone) - 1;
    self::$cant_array_parents = count($this->array_categorys_parent) - 1;
}

public function load(ObjectManager $manager)
{
    for ($i=0; $i < count($this->array_categorys) ; $i++) { 
        $category = new Category();
        $category->setName($this->array_categorys[$i]);
        /*Carga de Categorias Parent a base de datos*/
        switch ($this->array_categorys[$i]) {

            case self::LAPTOP :
            $manager->persist($category);
            $manager->flush();
            $this->addReference(self::LAPTOP, $category);
            break;

            case self::SMARTPHONE :
            $manager->persist($category);
            $manager->flush();
            $this->addReference(self::SMARTPHONE, $category);
            // $this->getReference(self::SMARTPHONE) ? null : $this->addReference(self::SMARTPHONE, $category);
            break;

            case self::CAMERA :
            $manager->persist($category);
            $manager->flush();
            $this->addReference(self::CAMERA, $category);
            // $this->getReference(self::CAMERA) ? null : $this->addReference(self::CAMERA, $category);
            break;

            case self::ACCESSORIE :
            $manager->persist($category);
            $manager->flush();
            $this->addReference(self::ACCESSORIE, $category);
            // $this->getReference(self::ACCESSORIE) ? null : $this->addReference(self::ACCESSORIE, $category);
            break;

            case self::HEADPHONE :
            $manager->persist($category);
            $manager->flush();
            $this->addReference(self::HEADPHONE, $category);
            // $this->getReference(self::HEADPHONE) ? null : $this->addReference(self::HEADPHONE, $category);
            break;
        }

        if ($this->array_categorys[$i] == self::LAPTOP) {
            if (count($this->sub_laptops) > 0) {
                for ($j=0; $j < count($this->sub_laptops) ; $j++) { 
                    $category = new Category();
                    $category->setName($this->sub_laptops[$j]);
                    $category->setParent($this->getReference(self::LAPTOP));
                    $manager->persist($category);
                    $manager->flush();
                    // $this->getReference(self::LAPTOP.'_'.$i) ? $this->setReference(self::LAPTOP . '_' . $i, $category) : $this->addReference(self::LAPTOP.'_'.$i, $category);
                    $this->addReference(self::LAPTOP . '_' . $j, $category);       
                }
            }
        }
        if ($this->array_categorys[$i] == self::SMARTPHONE) {
            if (count($this->sub_smartphones) > 0) {
                for ($j=0; $j < count($this->sub_smartphones) ; $j++) { 
                    $category = new Category();
                    $category->setName($this->sub_smartphones[$j]);
                    $category->setParent($this->getReference(self::SMARTPHONE));
                    $manager->persist($category);
                    $manager->flush();
                    $this->setReference(self::SMARTPHONE. '_' . $j, $category);   
                    // $this->getReference(self::SMARTPHONE.'_'.$i) ? null : $this->addReference(self::SMARTPHONE.'_'.$i, $category);    
                }
            }
        }
        if ($this->array_categorys[$i] == self::CAMERA) {
            if (count($this->sub_cameras) > 0) {
                for ($j=0; $j < count($this->sub_cameras) ; $j++) { 
                    $category = new Category();
                    $category->setName($this->sub_cameras[$j]);
                    $category->setParent($this->getReference(self::CAMERA));
                    $manager->persist($category);
                    $manager->flush();
                    $this->setReference(self::CAMERA.'_'.$j, $category); 
                    // $this->getReference(self::CAMERA.'_'.$i) ? null : $this->addReference(self::CAMERA.'_'.$i, $category);      
                }
            }
        }
        if ($this->array_categorys[$i] == self::ACCESSORIE) {
            if (count($this->sub_accessories) > 0) {
                for ($j=0; $j < count($this->sub_accessories) ; $j++) { 
                    $category = new Category();
                    $category->setName($this->sub_accessories[$j]);
                    $category->setParent($this->getReference(self::ACCESSORIE));
                    $manager->persist($category);
                    $manager->flush();
                    $this->setReference(self::ACCESSORIE.'_'.$j, $category);    
                    // $this->getReference(self::ACCESSORIE.'_'.$i) ? null : $this->addReference(self::ACCESSORIE.'_'.$i, $category);   
                }
            }
        }
        if ($this->array_categorys[$i] == self::HEADPHONE) {
            if (count($this->sub_headphone) > 0) {
                for ($j=0; $j < count($this->sub_headphone) ; $j++) { 
                    $category = new Category();
                    $category->setName($this->sub_headphone[$j]);
                    $category->setParent($this->getReference(self::HEADPHONE));
                    $manager->persist($category);
                    $manager->flush();
                    $this->setReference(self::HEADPHONE.'_'.$j, $category);  
                    // $this->getReference(self::HEADPHONE.'_'.$i) ? null : $this->addReference(self::HEADPHONE.'_'.$i, $category);     
                }
            }
        }


    }

}


}
