<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Inventory;
use Faker\Factory;

class InventoryFixtures extends Fixture
{
    public const INVENTORY_REFERENCE = 'inventory';
    public const CANT_INVENTORY_PRODUCT = 200;

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();
        for ($i=1; $i <= self::CANT_INVENTORY_PRODUCT ; $i++) { 
            $inventory = new Inventory(); 
            $inventory->setUnit('piece');
            $inventory->setAvailableQuantity($faker->randomNumber(3, true));
            $this->addReference(self::INVENTORY_REFERENCE . "_" . $i, $inventory);
            $manager->persist($inventory);
            $manager->flush();
        }
        
    }
}
