<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use App\Entity\User;
use App\DataFixtures\AppPerson;

class UserFixtures extends Fixture
{
    private $passwordEncoder;
    public const CANT_USER = 45;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)     {
        $this->passwordEncoder = $passwordEncoder;
    }

    public function load(ObjectManager $manager)
    {
        
            /*usuario super admin*/
            $user = new User();
            $user->setEmail('test.tiendae@gmail.com');
            $user->setPassword($this->passwordEncoder->encodePassword($user,'admin'));
            $user->setRoles(
                [
                    "ROLE_SUPER_ADMIN",
                ]
            );
            $user->setPerson($this->getReference(AppPerson::PERSON_USER_TEST));
            $manager->persist($user);
            $manager->flush();

        for ($i=1; $i <= self::CANT_USER ; $i++) { 
            $faker = \Faker\Factory::create();
            $user = new User();
            $user->setEmail($faker->email);
            $user->setPassword($this->passwordEncoder->encodePassword($user,'admin_'. $i));
            $user->setRoles(
                [
                    "ROLE_USER",
                ]
            );
            $user->setPerson($this->getReference(AppPerson::PERSON_USER_TEST . '_' . $i));
            $manager->persist($user);
            $manager->flush();
        }
    }

    public function getDependencies()
    {
        return [
            \AppPerson::class,
        ];
    }
}
