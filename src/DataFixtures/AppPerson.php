<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Person;

class AppPerson extends Fixture
{
    public const PERSON_USER_TEST = "person-user-test";
    public const CANT_PERSONS = 45;

    public function load(ObjectManager $manager)
    {
            $person = new Person();
            $person->setName("Gonzalo");
            $person->setLastName("Becchio");
            $person->setDni("35279270");
            $person->setPhoneAreaCode('3385');
            $person->setPhoneNumber('431980');
            $person->setAddress('Una calle me separa');
            $person->setAddressNumber('3030');
            $person->setZipCode('5000');
            $manager->persist($person);
            $manager->flush();
            $this->addReference(self::PERSON_USER_TEST, $person);

        for ($i=1; $i <= self::CANT_PERSONS ; $i++) {
            $faker = \Faker\Factory::create(); 
            $person = new Person();
            $person->setName($faker->name);
            $person->setLastName($faker->lastName);
            $person->setDni($faker->randomNumber(8, false));
            $person->setPhoneAreaCode($faker->randomNumber(4, false));
            $person->setPhoneNumber($faker->randomNumber(6, false));
            $person->setAddress($faker->streetName);
            $person->setAddressNumber($faker->buildingNumber);
            $person->setZipCode($faker->postcode);
            $manager->persist($person);
            $manager->flush();
            $this->addReference(self::PERSON_USER_TEST . '_' . $i, $person);
        }
    }
}
