<?php

namespace App\Repository;

use App\Entity\Receipt;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Receipt|null find($id, $lockMode = null, $lockVersion = null)
 * @method Receipt|null findOneBy(array $criteria, array $orderBy = null)
 * @method Receipt[]    findAll()
 * @method Receipt[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReceiptRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Receipt::class);
    }

    public function findLastId()
    {
        $limit = 1;
        return $this->createQueryBuilder('r')
                    ->orderBy('r.id','DESC')
                    ->setMaxResults($limit)
                    ->getQuery()
                    ->getOneOrNullResult()
        ;

    }

    /*Cantidad de Facturas Diarias*/
    /*Se obtienen entre las 00 hs y las 23:59:59*/
    public function totalReceipt($today)
    {
        $from = new \DateTime($today->format("Y-m-d")." 00:00:00");
        $to   = new \DateTime($today->format("Y-m-d")." 23:59:59");

        $qb = $this->createQueryBuilder('r')
                    ->setParameter('from', $from )
                    ->setParameter('to', $to)
        ;
                     $qb->add('select', 'count(r.id)')
                        ->add('where', $qb->expr()->between('r.data_emit_receipt', ':from', ':to'))
                     ;
        return       $qb->getQuery()
                        ->getSingleScalarResult()
        ;

        
        // $past = date_create('2022-03-03 15:19:13');
        // $now = date_create('now');    
    }



    // /**
    //  * @return Receipt[] Returns an array of Receipt objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Receipt
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
