<?php

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    public function sumAllPaymentsToday($today)
    {
        $from = new \DateTime($today->format('Y-m-d') . '00:00:00');
        $to = new \DateTime($today->format('Y-m-d') . '23:59:59');

        $qb = $this->createQueryBuilder('p')
                   ->select('sum(p.transaction_amount)')
                   ->setParameter('from', $from)          
                   ->setParameter('to', $to)
                   ;
        $qb->add('where',$qb->expr()->between('p.date_created', ':from', ':to'));
 
        return        $qb->getQuery()
                         ->getSingleScalarResult()          
                       ;
    }

    # {Ticket promedio ($) = Valor total de ventas ÷ cantidad de ventas #}
    public function averageTicket($today)
    {
        $from = new \DateTime($today->format('Y-m-d') . '00:00:00');
        $to = new \DateTime($today->format('Y-m-d') . '23:59:59');
        $qb = $this->createQueryBuilder('p')
                    ->select('avg(p.transaction_amount)')
                    ->setParameter('from', $from)
                    ->setParameter('to', $to)            
        ;
        $qb->add('where',$qb->expr()->between('p.date_created', ':from', ':to'));
        return $qb->getQuery()
                  ->getSingleScalarResult()
        ;

    }

    // /**
    //  * @return Payment[] Returns an array of Payment objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Payment
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
