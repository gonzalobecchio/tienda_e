<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Utilities\Pagination; 

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }


    /*Filtra las ordes por usuario logueado*/
    public function findOrdersByUser($user)
    {
        return $this->createQueryBuilder('o')
                    ->where('o.user = :user')
                    ->setParameter('user', $user)
                    ->getQuery()
                    ->getResult()
                    ;
    }

    public function findAllOrdersPaginator($page = 1, $search = null, $admin = false, $user = null)
    {
        $qb = $this->createQueryBuilder('o');
        if ($admin) {
            $qb->innerJoin('o.user','u','WITH', 'o.user = u.id')
               ->innerJoin('o.payment','p','WITH', 'o.payment = p.id')
            ;

            $qb->add('where', $qb->expr()->orX(
                $qb->expr()->like('p.id', $qb->expr()->literal('%'. $search .'%')),
                $qb->expr()->like('o.order_id', $qb->expr()->literal('%'. $search .'%')),
                $qb->expr()->like('u.email', $qb->expr()->literal('%'. $search .'%'))
            ))
            ;
        }else{
            $qb->where('o.user = :user')
               ->setParameter('user', $user)
            ;
        }

        $qb->getQuery();
        $pagination = new Pagination();
        $pagination->setPage($page);
        $paginator = $pagination->paginate($qb, $page);
        $pagination->setTotalRegister($paginator->count());
        $pagination->setCountPages(ceil($paginator->count() / pagination::ITEMBYPAGE));
        return 
            [
                'paginator' => $paginator,
                'pagination_object' => $pagination,
                'ITEMBYPAGE' => pagination::ITEMBYPAGE,
            ];

    }

    // /**
    //  * @return Order[] Returns an array of Order objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Order
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
