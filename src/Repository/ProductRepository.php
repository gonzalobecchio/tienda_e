<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Utilities\Pagination;   

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

    
    /*Busca todos los productos sin importar categoria y los pagina*/
    /* $front = true mostrar en front*/
    /* $front = false no mostrar en front - se muestra todo en panel administracion */
    public function findAllProductsPaginator($page, $front = false, $search = null)
    {    
        if ($front) {
            $qb = $this->createQueryBuilder('p')
                        ->where('p.status = :val')
                        ->orderBy('p.id', 'ASC')
                        ->setParameter('val', 'active')
                        ->getQuery()
            ;
        }else{
            $qb = $this->createQueryBuilder('p');
            if (null !== $search) {
                $qb->add('where', $qb->expr()->orX(
                    $qb->expr()->like('p.id', $qb->expr()->literal('%'. $search .'%')),
                    $qb->expr()->like('p.name', $qb->expr()->literal('%'. $search .'%')),
                    $qb->expr()->like('p.reference', $qb->expr()->literal('%'. $search .'%'))
                ));  
            }
            $qb->orderBy('p.id', 'ASC')        
               ->getQuery()
            ;
        }

        $pagination = new Pagination();
        $pagination->setPage($page);
        $paginator = $pagination->paginate($qb, $page);
        $pagination->setTotalRegister($paginator->count());
        $pagination->setCountPages(ceil($paginator->count() / pagination::ITEMBYPAGE));
        return 
            [
                'paginator' => $paginator,
                'pagination_object' => $pagination,
                'ITEMBYPAGE' => pagination::ITEMBYPAGE,
            ];
    }

    
    /*Busca todos los productos por categoria y los pagina*/
    public function findProductsByCategoryPaginator($id, $page, $ids_filter = null)
    {
        $pagination = new Pagination();
        $pagination->setPage($page);
        $qb = "";
        if ($ids_filter == null) {
            $qb = $this->createQueryBuilder('p')
            ->innerJoin('p.category','c','WITH', 'c.parent = :id')
            ->orderBy('p.id', 'ASC')
            ->setParameter('id', $id)
            ->getQuery();
            ;
        }else{
            $qb = $this->createQueryBuilder('p')
                       ->setParameter('active', 'active')
                       ->innerJoin('p.category','c');
            // $qb->add('where', $qb->expr()->eq('p.status', 'active'));
            // $qb->add('where', $qb->expr()->in('c.parent', $ids_filter));
            $qb->add('where', $qb->expr()->andX(
                $qb->expr()->eq('p.status', ':active'),
                $qb->expr()->in('c.parent', $ids_filter),
            ));
            $qb->orderBy('p.id', 'ASC')
                ->getQuery();
            ;
        }
        $paginator = $pagination->paginate($qb, $page);
        $pagination->setTotalRegister($paginator->count());
        $pagination->setCountPages(ceil($paginator->count() / pagination::ITEMBYPAGE));
        return 
        [
            'paginator' => $paginator,
            'pagination_object' => $pagination,
            'ITEMBYPAGE' => pagination::ITEMBYPAGE,
        ]
        ; 
    }

    /*Cantidad de productos por categoria padre*/
    public function findCantProductByParent()
    {
        return $this->createQueryBuilder('p')
        ->select('count(c.parent) as COUNT_BY_PARENT')
        ->addSelect('h.name')
        ->addSelect('h.id as ID')
        ->innerJoin('p.category', 'c')
        ->leftJoin('c.parent','h')
        ->groupBy('c.parent')
        ->getQuery()
        ->getResult()
        ;
    }

    public function findCategoryAndCant($id = null, $ids_categorys_filter = null)
    {
        /*Regresa el total de todas las categorias*/
        if (null == $id && $ids_categorys_filter == null) {

            $qb = $this->createQueryBuilder('p')
            ->getQuery()
            ->getResult()
            ; 
            return 
            [
               'id' => null,
               'count' => count($qb), 
           ];
       }


       /*Id_categorys_filter es null cuando ingresa sin ajax*/
       /*Regresa cantidad de productos por categoria*/
       if ($id != null && $ids_categorys_filter == null) {
        return $qb = $this->createQueryBuilder('p')
            ->select('count(c.parent) as COUNT_BY_PARENT')
            ->addSelect('h.name')
            ->innerJoin('p.category', 'c')
            ->leftJoin('c.parent','h')
            ->where('h.id = :id')
            ->groupBy('c.parent')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult()
        ;
        }

        /*Cuando ingresa por ajax id viene null*/
        /*Regresa la cantidad de productos por categorias seleccionadas*/
        if ($id == null && $ids_categorys_filter != null) {
                   $qb = $this->createQueryBuilder('p')
                        ->select('count(c.parent) as COUNT_CATEGORY_BY_FILTER')
                        ->innerJoin('p.category','c');
                    $qb->add('where', $qb->expr()->in('c.parent', $ids_categorys_filter));
                    
                    return $qb->getQuery()
                            ->getResult()
                    ;
                      
        }

    }

    public function findProductAsArray($id)
    {
        return $this->createQueryBuilder('p')
        ->where('p.id = :id')
        ->setParameter('id', $id)
        ->getQuery()
        ->getArrayResult()
        ;
    }
// SELECT id from product ORDER BY id DESC LIMIT 1; 
    public function findLastID()
    {
        return $this->createQueryBuilder('p')
                    ->orderBy('p.id', 'DESC')
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getSingleResult()
                    ;
    }

    public function totalPriceInventory()
    {
        return $this->createQueryBuilder('p')
                    ->select('sum(p.price * i.availableQuantity)')
                    ->innerJoin('p.inventory', 'i', 'WITH', 'p.inventory = i.id')
                    ->getQuery()
                    ->getSingleScalarResult()
        ; 
    }

    public function productsWithCrticalStock()
    {
        $qb =  $this->createQueryBuilder('p')
                    ->select('count(i.availableQuantity)')
                    ->innerJoin('p.inventory', 'i', 'WITH', 'p.inventory = i.id')
                    ;
        $qb->add('where',$qb->expr()->lt('i.availableQuantity', '100'));
        return      $qb->getQuery()
                       ->getSingleScalarResult()
                    ;
    }

    public function sumTotalStock()
    {
         return $this->createQueryBuilder('p')
                     ->select('sum(i.availableQuantity)')
                     ->innerJoin('p.inventory', 'i', 'WITH', 'p.inventory = i.id')
                     ->getQuery()
                     ->getSingleScalarResult()
                     ;
    }


    ///**
    //  * @return Product[] Returns an array of Product objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
