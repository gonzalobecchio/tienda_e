<?php

namespace App\Repository;

use App\Entity\OrderHasProducts;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method OrderHasProducts|null find($id, $lockMode = null, $lockVersion = null)
 * @method OrderHasProducts|null findOneBy(array $criteria, array $orderBy = null)
 * @method OrderHasProducts[]    findAll()
 * @method OrderHasProducts[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderHasProductsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, OrderHasProducts::class);
    }


    /*Retorna productos mas vendidos de la fecha y menos vendidos de la fecha segun el parametro ORDER ingresado*/
    public function ordersSelling($today, $order = 'ASC')
    {
        $from = new \DateTime($today->format('Y-m-d') . '00:00:00');
        $to = new \DateTime($today->format('Y-m-d') . '23:59:59');

        $qb =   $this->createQueryBuilder('h')
                     ->select('h','count(h)')
                     ->innerJoin('h.orders', 'o', 'WITH', 'h.orders = o.id')
                     ->groupBy('h.products')
                     ->orderBy('h.products',$order)
                     ->addOrderBy('o.createdAt')
                     ->setMaxResults(5)
                     ->setParameter('from', $from)
                     ->setParameter('to', $to)
                     ;
        $qb->add('where',
             $qb->expr()->between('o.createdAt',':from', ':to'
         ));
        
        return  $qb->getQuery()
                   ->getResult()
                ;
    }

    // /**
    //  * @return OrderHasProducts[] Returns an array of OrderHasProducts objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('o.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?OrderHasProducts
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
