<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\PasswordUpgraderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Utilities\Pagination;   

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements PasswordUpgraderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * Used to upgrade (rehash) the user's password automatically over time.
     */
    public function upgradePassword(UserInterface $user, string $newHashedPassword): void
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', \get_class($user)));
        }

        $user->setPassword($newHashedPassword);
        $this->_em->persist($user);
        $this->_em->flush();
    }

    public function findEmailUserLog($user)
    {
        /*En la construccion de un form se debe devolver un QueryBuilder object*/
        return $this->createQueryBuilder('u')
                    ->select('u')
                    ->where('u.id = :user')
                    ->setParameter('user', $user)
                    ;
    }

    /*Devuelve Array de Usuarios*/
    public function findAllUserArray()
    {
        return $this->createQueryBuilder('u')
                    ->select('u')
                    ->getQuery()
                    ->getArrayResult();
        ;
    }

    /*Devuelve todos los usuarios menos el user log con permisos super admin*/
    public function findAllNeqUserLoged($user, $page = 1, $search = false, $busqueda = null)
    {
        if (!$search) {
            $qb =  $this->createQueryBuilder('u')
                        ->select('u')
                        ->setParameter('user', $user)
            ;
            $qb->add('where', $qb->expr()->neq('u', ':user'));
        }else{
            $qb =  $this->createQueryBuilder('u')
                        ->select('u')
                        ->setParameter('user', $user)
            ;
            $qb->add('where', $qb->expr()->andX(
                $qb->expr()->neq('u', ':user'),
                $qb->expr()->like('u.email', $qb->expr()->literal('%'. $busqueda .'%'))
            ));
        }
        
        $qb->orderBy('u.id', 'ASC');
        $pagination = new Pagination();
        $paginator = $pagination->paginate($qb->getQuery(), $page);
        $pagination->setTotalRegister($paginator->count());
        $pagination->setCountPages(ceil($paginator->count() / pagination::ITEMBYPAGE));
        return 
        [
            'paginator' => $paginator,
            'pagination_object' => $pagination,
            'ITEMBYPAGE' => pagination::ITEMBYPAGE,
        ];
    }

    // /**
    //  * @return User[] Returns an array of User objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('u.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
