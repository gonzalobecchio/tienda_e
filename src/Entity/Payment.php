<?php

namespace App\Entity;

use App\Repository\PaymentRepository;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Mime\Address;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 * @ORM\Entity(repositoryClass=PaymentRepository::class)
 */
class Payment
{

    const IVA = 0.21;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     * @ORM\Column(type="string")
     */
    private $id;
   
    /**
     * @ORM\Column(type="string")
     */
    private $external_reference;

     /**
     * @ORM\Column(type="datetime", name="date_created")  
     * @Gedmo\Timestampable(on="create")
     */
    private $date_created;

    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(type="string")
     */
    private $transaction_amount;

    /*Array con datos de API PAYMENT*/
    private $payment;

    /*Array con items de payments*/
    private $items;

    /*Cuotas de payments*/
    private $installments;

    /*Order MP*/
    private $order_id;

    /*Addres MP Envio*/
    private $address;

    private $payment_method_id;

    /*Array datos CARD*/
    private $card;

    /*Detalles Transaccion*/
    private $transaction_details;

    private $status_detail;

    private $payer;

    function __construct($payment_id = null)
    {
        if ($payment_id !== null) {
           $this->payment = json_decode($this->searchPayment($payment_id), true);
            // dump($this->payment); die;
           $this->set($this->payment);

        }
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function setId($id): ?string
    {
        $this->id = $id;
        
        return $this->id;
    }

    public function set($payment = null)
    {
        if (!isset($payment) || !is_array($payment)) {
            return null;
        }

        $this->id = isset($payment['id']) ? $payment['id'] : null ;
        $this->external_reference = isset($payment['external_reference']) ? $payment['external_reference'] : null;
        $this->status = isset($payment['status']) ? $payment['status'] : null;
        $this->status_detail = isset($payment['status_detail']) ? $payment['status_detail'] : null;
        $this->transaction_amount = isset($payment['transaction_amount']) ? $payment['transaction_amount'] : null;
        $this->items = isset($payment['additional_info']['items']) && is_array($payment['additional_info']['items']) ? $payment['additional_info']['items'] : null;
        $this->installments = isset($payment['installments']) ? $payment['installments'] : null; 
        $this->order_id = isset($payment['order']) && is_array($payment['order']) ? $payment['order']['id'] : null;
        $this->address = isset($payment['additional_info']['payer']['address']) && is_array($payment['additional_info']['payer']['address']) ? $payment['additional_info']['payer']['address'] : null;
        $this->payment_method_id = isset($payment['payment_method_id']) ? $payment['payment_method_id'] : null;
        $this->card = isset($payment['card']) && is_array($payment['card']) ? $payment['card'] : null ;
        $this->transaction_details = isset($payment['transaction_details']) && is_array($payment['transaction_details']) ? $payment['transaction_details'] : null;
        $this->payer = isset($payment['payer']) && is_array($payment['payer']) ? $payment['payer'] : null;
    }
    
     /**
     * @param mixed $payment_id
     *
     * @return self
     */
    public function setPaymentId($payment_id)
    {
        $this->payment_id = $payment_id;

        return $this;
    }

    public function getPaymentId(): ?string  
    {
        return $this->payment_id;
    }

    public function getExternalReference(): ?string
    {
        return $this->external_reference;
    }

    public function setExternalReference(string $external_reference): self
    {
        $this->external_reference = $external_reference;

        return $this;
    }

    public function getDateCreated(): ?string
    {
        return $this->date_created;
    }

    public function setDateCreated(string $date_created): self
    {
        $this->date_created = $date_created;

        return $this;
    }


    /*Obtiene detalles del pago*/
    /*Return json con los detalles pago*/
    public function searchPayment($payment_id)
    {
        $url = "https://api.mercadopago.com/v1/payments/". $payment_id;
        $headers = 
            [
                'Authorization: Bearer TEST-4515115309737584-091414-ac8f240e204d7a5f251fe90fc990a703-824230212',
            ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$headers);
        if (curl_exec($ch) === false) {
            return "Code Error: " . curl_errno($ch) . "-" . curl_strerror(curl_errno($ch));
        }

        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTransactionAmount(): ?string
    {
        return $this->transaction_amount;
    }

    public function setTransactionAmount(string $transaction_amount): self
    {
        $this->transaction_amount = $transaction_amount;

        return $this;
    }

    public function sendEmailPayment(MailerInterface $mailer, User $user, $payment, $filePdfReceipt)
    {
        if (isset($payment) && isset($user)) {
            /*Realizar la configuracion de envio y crear el email con estilos*/
            // dump($user);
            // dump($data_payment_mp_api); die;
            $email = (new TemplatedEmail())
            ->from( new Address('gonzalobecchio@gmail.com', 'Tienda E!'))
            ->to($user->getEmail())
                    //->cc('cc@example.com')
                    //->bcc('bcc@example.com')
                    //->replyTo('fabien@example.com')
                    //->priority(Email::PRIORITY_HIGH)
            ->subject('Resumen compra y factura Tienda e! - #' . $payment->getId())
            ->htmlTemplate('mailer/email_payment_success.html.twig')
            ->context([
                'user' => $user,
                'payment' => $payment,
            ])
            ->attachFromPath(dirname( __DIR__, 2) . '/receipt/' . date('Y') . '/' . $filePdfReceipt)
            ;

            try {
                $mailer->send($email);
            } catch (TransportExceptionInterface $e) {

            } 
        }
        
    }


    /**
     * @return mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param mixed $order_id
     *
     * @return self
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    public function fullAddressShiping()
    {
        return $this->getAddress()['street_name'] . "" . $this->getAddress()['street_number'];
    }

    /**
     * @return mixed
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param mixed $items
     *
     * @return self
     */
    public function setItems($items)
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTransactionDetails()
    {
        return $this->transaction_details;
    }

    /**
     * @param mixed $transaction_details
     *
     * @return self
     */
    public function setTransactionDetails($transaction_details)
    {
        $this->transaction_details = $transaction_details;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPaymentMethodId()
    {
        return $this->payment_method_id;
    }

    /**
     * @param mixed $payment_method_id
     *
     * @return self
     */
    public function setPaymentMethodId($payment_method_id)
    {
        $this->payment_method_id = $payment_method_id;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCard()
    {
        return $this->card;
    }

    /**
     * @param mixed $card
     *
     * @return self
     */
    public function setCard($card)
    {
        $this->card = $card;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstallments()
    {
        return $this->installments;
    }

    /**
     * @param mixed $installments
     *
     * @return self
     */
    public function setInstallments($installments)
    {
        $this->installments = $installments;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getStatusDetail()
    {
        return $this->status_detail;
    }

    /**
     * @param mixed $status_detail
     *
     * @return self
     */
    public function setStatusDetail($status_detail)
    {
        $this->status_detail = $status_detail;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getPayer()
    {
        return $this->payer;
    }

    /**
     * @param mixed $payer
     *
     * @return self
     */
    public function setPayer($payer)
    {
        $this->payer = $payer;

        return $this;
    }

    private $iva_num;
    private $totalWithIva;

    /**
     * @return mixed
     */
    public function getTotalWithIva()
    {
        return $this->transaction_amount + $this->iva_num;
    }

    /**
     * @param mixed $totalWithIva
     *
     * @return self
     */
    public function setTotalWithIva($totalWithIva)
    {
        $this->totalWithIva = $totalWithIva;

        return $this;
    }

    /**
     * @param mixed $iva_num
     *
     * @return self
     */
    public function setIvaNum()
    {
        $this->iva_num = $this->getTransactionAmount() * self::IVA;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getIvaNum()
    {
        $this->iva_num = $this->transaction_amount * self::IVA;
        return $this->transaction_amount * self::IVA;
    }
}
