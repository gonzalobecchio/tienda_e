<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use App\Entity\Person;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @UniqueEntity("email",
 *      fields = {"email"}, 
 *      errorPath = "email",
 *      message = "This email: {{ value }} is registered"  
 * )

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique="true")
     * @Assert\Email(
     *      message = "The email '{{ value }}' is not a valid email.",
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];    

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     * @Assert\NotNull
     * @Assert\Length(
     *      min="6", 
     *      max="10",
     *      minMessage = "Your Password must be at least {{ limit }} characters long",
     *      maxMessage = "Your Password name cannot be longer than {{ limit }} characters",
     *      allowEmptyString = false,
     *)
     * @Assert\Type(
     *      type={"alpha", "digit"},
     *      message = "This value should be of type {{ type }}",
     *)
     */
    private $password;

    /**
     * @ORM\OneToOne(targetEntity="Person")
     */
    private $person;

    
    /**
     * @Assert\Valid
     */
    protected $persons;

    /**
     * @ORM\OneToMany(targetEntity="Order", mappedBy="user")
     */
    private $orders;

    /**
     * @ORM\Column(type="string", length="40", nullable="true", unique="true")
     */
    private $payerMP_id;

    protected $roles_avaibles = 
        [
            "ROLE_USER",
            "ROLE_ADMIN",
            "ROLE_SUPER_ADMIN",
        ];

    /**
     * @ORM\OneToMany(targetEntity="Receipt", mappedBy="user")
     */
    private $receipt;

    public function __construct()
    {
        /*Form embebed Person*/
        $this->persons = new ArrayCollection();
        $this->orders = new ArrayCollection();
        $this->receipt = new ArrayCollection();        
    }

    public function getPersons(): Collection
    {
        return $this->persons;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        // $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Returning a salt is only needed, if you are not using a modern
     * hashing algorithm (e.g. bcrypt or sodium) in your security.yaml.
     *
     * @see UserInterface
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    /**
     * @return Collection|Order[]
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function addOrder(Order $order): self
    {
        if (!$this->orders->contains($order)) {
            $this->orders[] = $order;
            $order->setUser($this);
        }

        return $this;
    }

    public function removeOrder(Order $order): self
    {
        if ($this->orders->removeElement($order)) {
            // set the owning side to null (unless already changed)
            if ($order->getUser() === $this) {
                $order->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @param mixed $persons
     *
     * @return self
     */
    public function setPersons($persons)
    {
        $this->persons = $persons;

        return $this;
    }

    public function getPayerMPId(): ?string
    {
        return $this->payerMP_id;
    }

    public function setPayerMPId(?string $payerMP_id): self
    {
        $this->payerMP_id = $payerMP_id;

        return $this;
    }

    /*Funcion utilizada von DataTable en el controladorm PanelAdminController*/
    // public function selectedRole($key_value)
    // {
    //     if (!is_numeric($key_value)) {
    //         $new_array = $this->roles_avaibles;
    //         foreach ($new_array as $key) {
    //             if (array_key_exists($key_value, $new_array)) {
    //                 $new_array[$key_value] = null;
    //             }
    //         } 
    //         // dump($new_array); die;
    //         return $new_array;
    //     }else{
    //         // dump('Ingreso por numerico'); die;
    //         $new_array = array_keys($this->roles_avaibles);
    //         $new_array[] = $new_array[$key_value];
    //         $other_array[$new_array[$key_value]] = null ;  
    //         // dump($other_array); die;
    //         return $other_array;

    //     }

    // }

    public function selectedRole($key_value)
    {
        // $new_array = $this->roles_avaibles;
        // dump($this->roles_avaibles);die;
        foreach ($this->roles_avaibles as $key => $value) {
            $new_array[$value] = $value;
        } 
        // dump($new_array);die;
        return $new_array;
    }

    /**
     * @return Collection|Receipt[]
     */
    public function getReceipt(): Collection
    {
        return $this->receipt;
    }

    public function addReceipt(Receipt $receipt): self
    {
        if (!$this->receipt->contains($receipt)) {
            $this->receipt[] = $receipt;
            $receipt->setUser($this);
        }

        return $this;
    }

    public function removeReceipt(Receipt $receipt): self
    {
        if ($this->receipt->removeElement($receipt)) {
            // set the owning side to null (unless already changed)
            if ($receipt->getUser() === $this) {
                $receipt->setUser(null);
            }
        }

        return $this;
    }

}
