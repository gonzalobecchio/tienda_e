<?php

namespace App\Entity;

use App\Repository\OrderHasProductsRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 * @ORM\Entity(repositoryClass=OrderHasProductsRepository::class)
 */
class OrderHasProducts
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"}, inversedBy="hasOrder")
     */
    private $products;

    /**
     * @ORM\ManyToOne(targetEntity="Order", cascade={"persist","remove"}, inversedBy="hasProduct")
     */
    private $orders;

    /**
     * @ORM\Column(type="integer")
     * @Assert\NotBlank
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", nullable="true");
     */
    private $reserved;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getProducts(): ?Product
    {
        return $this->products;
    }

    public function setProducts(?Product $products): self
    {
        $this->products = $products;

        return $this;
    }

    public function getOrders(): ?Order
    {
        return $this->orders;
    }

    public function setOrders(?Order $orders): self
    {
        $this->orders = $orders;

        return $this;
    }

    public function getReserved(): ?string
    {
        return $this->reserved;
    }

    public function setReserved(?string $reserved): self
    {
        $this->reserved = $reserved;

        return $this;
    }

    public function createOrderHasProducts($data)
    {
        $this->setProducts($data['product']);
        $this->setOrders($data['order']);
        $this->setQuantity($data['qty']);
        $this->setReserved($data['reserved']);
        return true;
    }
}
