<?php

namespace App\Entity;

use App\Repository\PersonRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations

/**
 * @ORM\Entity(repositoryClass=PersonRepository::class)
 *
 * @UniqueEntity(
 *      fields = {"dni"}, 
 *      errorPath = "dni",
 *      message = "This dni: {{ value }} is registered",
 *      entityClass = "App\Entity\Person",
 * )
 @UniqueEntity(
 *      fields = {"phone_number"}, 
 *      errorPath = "phone_number",
 *      message = "This phone_number: {{ value }} is registered",
 *      entityClass = "App\Entity\Person",
 * )
 *
 */
class Person
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(length="40")
     * @Assert\NotBlank(message="The value {{ value }} should not be blank")
     * @Assert\NotNull(message="The value {{ value }} should not be null")
     * @Assert\Type(type={"alpha"}, message="This value {{ value }} should be of type {{ type }}")
     * @Assert\Length(
     *            min = "1", 
     *            max = "40",
     *            minMessage = "Your name must be at least {{ limit }} characters long", 
     *            maxMessage = "Your name cannot be longer than {{ limit }} characters",
     *            allowEmptyString = false,
     *            )
     */
    private $name;

    /**
     * @ORM\Column(length="40")
     * @Assert\NotBlank(message="The value {{ value }} should not be blank")
     * @Assert\NotNull(message="The value {{ value }} should not be null")
     * @Assert\Type(type={"alpha"}, message="This value {{ value }} should be of type {{ type }}")
     * @Assert\Length(
     *            min = "1", 
     *            max = "40",
     *            minMessage = "Your Last Name must be at least {{ limit }} characters long", 
     *            maxMessage = "Your Last Name cannot be longer than {{ limit }} characters",
     *            allowEmptyString = false,
     *            )
     *
     *
     */
    private $lastName;

    
    /**
     * @ORM\Column(name="dni", length="10", unique="true")
     * @Assert\NotBlank(message="The value {{ value }} should not be blank")
     * @Assert\NotNull(message="The value {{ value }} should not be null")
     * @Assert\Type(type={"digit"}, message="This value {{ value }} should be of type {{ type }}")
     * @Assert\Length(
     *            min = "7", 
     *            max = "8",
     *            minMessage = "Your DNI  must be at least {{ limit }} characters long", 
     *            maxMessage = "Your DNI cannot be longer than {{ limit }} characters",
     *            allowEmptyString = false,
     *            )
     *
     */
    public $dni;

    /**
     * @ORM\Column(type="string")
     */
    private $phone_area_code;

    /**
     * @ORM\Column(type="string", unique="true")
     */
    private $phone_number;

    /**
     * @ORM\Column(name="address", length="40")
     * @Assert\NotBlank(message="The value {{ value }} should not be blank")
     * @Assert\NotNull(message="The value {{ value }} should not be null")
     */
    private $address;

    /**
     * @ORM\Column(length="10")
     * @Assert\Positive
     */
    private $address_number;

    /**
     * @ORM\Column(length="6")
     * @Assert\Positive
     */
    private $zip_code;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="update_at")
     * @Gedmo\Timestampable(on="update")
     */
    private $updateAt;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getDni(): ?string
    {
        return $this->dni;
    }

    public function setDni(?string $dni): self
    {
        $this->dni = $dni;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     *
     * @return self
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    

    public function getZipCode(): ?string
    {
        return $this->zip_code;
    }

    public function setZipCode(?string $zip_code): self
    {
        $this->zip_code = $zip_code;

        return $this;
    }

    public function getAddressNumber(): ?string
    {
        return $this->address_number;
    }

    public function setAddressNumber(?string $address_number): self
    {
        $this->address_number = $address_number;

        return $this;
    }

    public function getPhoneAreaCode(): ?string
    {
        return $this->phone_area_code;
    }

    public function setPhoneAreaCode(string $phone_area_code): self
    {
        $this->phone_area_code = $phone_area_code;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    public function fullName()
    {
        return "{$this->getName()} {$this->getLastName()}"; 
    }

    public function fullAddress()
    {
        return "{$this->getAddress()} {$this->getAddressNumber()}";
    }

    public function fullPhoneNumber()
    {
        return "{$this->getPhoneAreaCode()} - {$this->getPhoneNumber()}";
    }

}
