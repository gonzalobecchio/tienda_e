<?php

namespace App\Entity;

use App\Repository\OrderRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User" , inversedBy="orders")
     */
    private $user;
    
    /**
     * @ORM\Column(length="40", nullable="true")
     */
    private $status;
    
    /**
     * @ORM\Column(length="50" , nullable="true")
     */
    private $shipping_address;

    /**
     * @ORM\OneToOne(targetEntity="Payment")
     * @ORM\JoinColumn(name="payment_id", referencedColumnName="id")
     */
    private $payment;

    private $payment_paint_search;

    /**
     * @ORM\Column(type="string", name="order_mp")
     */
    private $order_id;

    /**
     * @ORM\Column(type="datetime", name="created_at")
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\OneToMany(targetEntity="OrderHasProducts", mappedBy="orders" , cascade={"persist", "remove"})
     */
    private $hasProduct;

    /**
     * @ORM\OneToOne(targetEntity="Receipt")
     */
    private $Receipt;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function __construct()
    {
        $this->hasProduct = new ArrayCollection();
    }



    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getShippingAddress(): ?string
    {
        return $this->shipping_address;
    }

    public function setShippingAddress(string $shipping_address): self
    {
        $this->shipping_address = $shipping_address;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|OrderHasProducts[]
     */
    public function getHasProduct(): Collection
    {
        return $this->hasProduct;
    }

    public function addHasProduct(OrderHasProducts $hasProduct): self
    {
        if (!$this->hasProduct->contains($hasProduct)) {
            $this->hasProduct[] = $hasProduct;
            $hasProduct->setOrders($this);
        }

        return $this;
    }

    public function removeHasProduct(OrderHasProducts $hasProduct): self
    {
        if ($this->hasProduct->removeElement($hasProduct)) {
            // set the owning side to null (unless already changed)
            if ($hasProduct->getOrders() === $this) {
                $hasProduct->setOrders(null);
            }
        }

        return $this;
    }

    public function getPayment(): ?Payment
    {
        return $this->payment;
    }

    public function setPayment(?Payment $payment): self
    {
        $this->payment = $payment;

        return $this;
    }


    public function getOrderId(): ?string
    {
        return $this->order_id;
    }

    public function setOrderId(string $order_id): self
    {
        $this->order_id = $order_id;

        return $this;
    }

    public function createOrder($data)
    {
        $this->setUser($data['user']);
        $this->setPayment($data['payment']);
        /*Estado del Envio*/
        $this->setStatus("PENDING");
        $this->setShippingAddress($data['payment']->fullAddressShiping());
        $this->setOrderId($data['payment']->getOrderId());
    }

    public function getReceipt(): ?Receipt
    {
        return $this->Receipt;
    }

    public function setReceipt(?Receipt $Receipt): self
    {
        $this->Receipt = $Receipt;

        return $this;
    }




    /**
     * @return mixed
     */
    public function getPaymentPaintSearch()
    {
        return $this->payment_paint_search;
    }

    /**
     * @param mixed $payment_paint_search
     *
     * @return self
     */
    public function setPaymentPaintSearch($payment_paint_search)
    {
        $this->payment_paint_search = $payment_paint_search;

        return $this;
    }
}
