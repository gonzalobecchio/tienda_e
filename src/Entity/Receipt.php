<?php

namespace App\Entity;

use App\Repository\ReceiptRepository;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations
/*Biblioteca creacion PDF*/
use Dompdf\Dompdf;
use Dompdf\Options;

use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

/**
 * @ORM\Entity(repositoryClass=ReceiptRepository::class)
 */
class Receipt
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique="true")
     */
    private $number_receipt;

    
    /*Fecha de Emision*/
    /**
     * @ORM\Column(type="datetime")
     * @Gedmo\Timestampable(on="create")
     */
    private $data_emit_receipt;

    /*URL archivo pdf factura*/
    /**
     * @ORM\Column(type="string", nullable="true")
     */
    private $file_pdf_receipt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="receipt")
     * 
     */
    private $user;

    function __construct()
    {
        $this->user = new ArrayCollection();
    }

    public function getNumberReceipt(): ?string
    {
        return $this->number_receipt;
    }

    public function getDataEmitReceipt(): ?\DateTimeInterface
    {
        return $this->data_emit_receipt;
    }

    public function setDataEmitReceipt(\DateTimeInterface $data_emit_receipt): self
    {
        $this->data_emit_receipt = $data_emit_receipt;

        return $this;
    }

    public function getFilePdfReceipt(): ?string
    {
        return $this->file_pdf_receipt . '.pdf';
    }

    public function setFilePdfReceipt(string $file_pdf_receipt): self
    {
        $this->file_pdf_receipt = $file_pdf_receipt;

        return $this;
    }

    /*Creacion de numero de factura*/
    public function generateNumberReceipt($lastReceipt)
    {
        if (is_null($lastReceipt)) {
            return '2022/1';
        }
        
        return '2022/' . ($lastReceipt->getId() + 1);
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setNumberReceipt(string $number_receipt): self
    {
        $this->number_receipt = $number_receipt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }


    public function createReceiptPdf($response)
    {
       $options = new Options();
       $options->set('isRemoteEnabled', true);
       $options->set('chroot', '/');
       $dompdf = new Dompdf($options);
       $html = $response->getContent();
       $dompdf->loadHtml($html);
       $dompdf->setPaper('letter', 'portrait');
       $dompdf->render();
       return $dompdf->output();
    }

    public function nameFilePDF()
    {
        return uniqid(date('Y'));
    }

    public function storeReceiptPdf($pdf, $pdfOriginalName)
    {
        $file_sys = new Filesystem();
        /*Sino existe directorio lo crea*/
        if (!$file_sys->exists(realpath(dirname( __DIR__, 2) . '\/receipt\/' . date('Y')))) {
            $file_sys->mkdir(dirname( __DIR__, 2) . '\/receipt\/' . date('Y'), 0777);
        }
        try {
         $file_sys->dumpFile(dirname( __DIR__, 2) . '\/receipt\/'. date('Y') . '/' . $pdfOriginalName, $pdf);      
        } catch (IOException $e) {
            $e->getPath();
        }
    }

    // public function openPdf($fileName)
    // {
    //     $file_sys = new Filesystem();
    //     if (!$file_sys->exists(realpath(dirname( __DIR__, 2) . '\/receipt\/' . date('Y')))) {
    //         exit('the current directory or file does not exist');
    //     } 
    //     $file  = new File(realpath(dirname( __DIR__, 2) . '\/receipt\/' . date('Y')) . '/' . $fileName);
    //     return $this->file()
    // }



}
