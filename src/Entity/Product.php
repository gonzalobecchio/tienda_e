<?php

namespace App\Entity;

use App\Repository\ProductRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Gedmo\Mapping\Annotation as Gedmo; // gedmo annotations


/**
 * @ORM\Entity(repositoryClass=ProductRepository::class)
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(length="40")
     * @Assert\NotBlank(message="The value {{ value }} should not be blank")
     * @Assert\NotNull(message="The value {{ value }} should not be null")
     * @Assert\Length(
     *            min = "1", 
     *            max = "40",
     *            minMessage = "Your Name must be at least {{ limit }} characters long", 
     *            maxMessage = "Your Name cannot be longer than {{ limit }} characters",
     *            allowEmptyString = false,
     *            )
     */
    private $name;

    /**
     * @ORM\Column(type="text", length="40")
     * @Assert\NotBlank(message="The value {{ value }} should not be blank")
     * @Assert\NotNull(message="The value {{ value }} should not be null")
     * @Assert\Length(
     *            min = "20", 
     *            max = "400",
     *            minMessage = "Your Description must be at least {{ limit }} characters long", 
     *            maxMessage = "Your Description cannot be longer than {{ limit }} characters",
     *            allowEmptyString = false,
     *            )
     */
    private $description;


    /*Available - unevailable*/
    /**
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @ORM\Column(length="40")
     * @Assert\Length(
     *            min = "6", 
     *            max = "15",
     *            minMessage = "Your Reference must be at least {{ limit }} characters long", 
     *            maxMessage = "Your Reference cannot be longer than {{ limit }} characters",
     *            allowEmptyString = false,
     *            )
     */
    private $reference;

    /**
     * @ORM\Column(type="float", length="10")
     * @Assert\NotBlank
     * @Assert\NotNull
     */
          
    private $price;

        
    /**
     * @ORM\ManyToOne(targetEntity="Category", fetch="EAGER")
     */
    private $category;

    /**
     * @ORM\Column(type="json")
     */
    private $images;

   
    /*ArrayColecction de FormType Inventary*/
    /**
      * @Assert\Valid() 
      */
    protected $inventory_collection;


    // Relacion con Obj Inv
    /**
     * @ORM\OneToOne(targetEntity="Inventory", cascade={"persist"}, fetch="EAGER")
     */
    private $inventory;

    //  *
    //  * @OneToMany(targetEntity="Feature", mappedBy="product", cascade={"persist"})
     
    // private $features;
   

    /**
     * @ORM\Column(type="datetime", name="create_at")  
     * @Gedmo\Timestampable(on="create")
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", name="update_at")
     * @Gedmo\Timestampable(on="update")
     */
    private $updateAt;

    /**
     * @ORM\OneToMany(targetEntity="OrderHasProducts", mappedBy="products", cascade={"persist", "remove"})
     */
    private $hasOrder;

    /**
      * @ORM\Column(type="integer")
      * @Assert\NotNull
      * @Assert\PositiveOrZero;
      * @Assert\Range(
      *     min=0,
      *     max=100,
      *     notInRangeMessage = "You must be between {{ min }} sale  and {{ max }} sale to enter",
      *)
      */
    private $sales;

    /**
     * @ORM\Column(type="boolean", nullable="true")
     */
    private $isNew;

    private $idString;

    function __construct()
    {
        $this->features = new ArrayCollection();
        $this->hasOrder = new ArrayCollection();
        $this->inventory_collection = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /*Uso solo para marcado de busqueda*/
    public function setIdSearch($id)
    {
         $this->id = $id;

         return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription($description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus($status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference($reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice($price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getImages(): ?array
    {
        return $this->images;
    }

    public function setImages(array $images): self
    {
        $this->images = $images;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdateAt(): ?\DateTimeInterface
    {
        return $this->updateAt;
    }

    public function setUpdateAt(\DateTimeInterface $updateAt): self
    {
        $this->updateAt = $updateAt;

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /*Calculo descuento*/
    public function getSalePrice()
    {
        if (!$this->getSales()) {
            return;
        }

        $value = $this->getPrice() && $this->getSales() ? 
                                    $this->getPrice() - (($this->getSales() * $this->getPrice()) / 100)  
                                 :  null;

        return round($value,2);
    }

    public function getSales(): ?int
    {
        return $this->sales;
    }

    public function setSales(?int $sales): self
    {
        $this->sales = $sales;

        return $this;
    }

    public function getIsNew(): ?bool
    {
        return $this->isNew;
    }

    public function setIsNew(?bool $isNew): self
    {
        $this->isNew = $isNew;

        return $this;
    }

    /**
     * @return Collection|OrderHasProducts[]
     */
    public function getHasOrder(): Collection
    {
        return $this->hasOrder;
    }

    public function addHasOrder(OrderHasProducts $hasOrder): self
    {
        if (!$this->hasOrder->contains($hasOrder)) {
            $this->hasOrder[] = $hasOrder;
            $hasOrder->setProducts($this);
        }

        return $this;
    }

    public function removeHasOrder(OrderHasProducts $hasOrder): self
    {
        if ($this->hasOrder->removeElement($hasOrder)) {
            // set the owning side to null (unless already changed)
            if ($hasOrder->getProducts() === $this) {
                $hasOrder->setProducts(null);
            }
        }

        return $this;
    }


    

    public function getInventory(): ?Inventory
    {
        return $this->inventory;
    }

    public function setInventory(?Inventory $inventory): self
    {
        $this->inventory = $inventory;

        return $this;
    }

 
    public function getInventoryCollection()
    {
        return $this->inventory_collection;
    }

    public function setInventoryCollection(ArrayCollection $inventory_collection)
    {
         $this->inventory_collection = $inventory_collection;
    }


    public function getFeatures()
    {
        return $this->features;
    }


    public function createReference(string $parent, $lastID)
    {
        if (is_string($parent) && isset($parent)) {
            $this->setReference(strtoupper(substr($parent, 0, 3)) . '-' . $lastID);
        }
    }

    /**
     * @return mixed
     */
    public function getIdString()
    {
        return $this->idString;
    }

    /**
     * @param mixed $idString
     *
     * @return self
     */
    public function setIdString($idString)
    {
        $this->idString = $idString;

        return $this;
    }
}
