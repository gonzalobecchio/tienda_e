<?php

namespace App\Entity;

use App\Repository\InventoryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InventoryRepository::class)
 */
class Inventory
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /*Indica pieza (ej: PC o Promocion 2 PC)*/
    /**
     * @ORM\Column(length="40", nullable="true")
     * 
     */
    private $unit;

    /**
     * @ORM\Column(type="integer", length="10"))
     * @Assert\NotBlank
     * @Assert\NotNull
     */
    private $availableQuantity;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUnit(): ?string
    {
        return $this->unit;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    public function getAvailableQuantity(): ?int
    {
        return $this->availableQuantity;
    }

    public function setAvailableQuantity($availableQuantity)
    {
        $this->availableQuantity = $availableQuantity;

        return $this;
    }


    public function decrementQty(int $qty)
    {
        if (!$qty) {
            return null;
        }

        $this->setAvailableQuantity($this->getAvailableQuantity() - $qty);
    }
}
