<?php  

namespace Utilities;

use Doctrine\ORM\Tools\Pagination\Paginator;

/**
 * 
 */
class Pagination 
{
	const ITEMBYPAGE = 15;
	/*pagina actual*/
	private $page;
	/*cantidad de paginas */
	private $count_pages;
	/*cantidad de registros de consulta*/
	private $total_register;

	
	function __construct()
	{
		$this->page = 1;
        $this->total_register = 0;
        $this->count_pages = 0;
        
    }


    /**
     * @return mixed
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param mixed $page
     *
     * @return self
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountPages()
    {
        return $this->count_pages;
    }

    /**
     * @param mixed $count_pages
     *
     * @return self
     */
    public function setCountPages($count_pages)
    {
        $this->count_pages = $count_pages;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getTotalRegister()
    {
        return $this->total_register;
    }

    /**
     * @param mixed $total_register
     *
     * @return self
     */
    public function setTotalRegister($total_register)
    {
        $this->total_register = $total_register;

        return $this;
    }

    public function firstResult()
    {
        return ($this->getPage() - 1) * self::ITEMBYPAGE;
    }

    

    /*Return object de tipo paginator*/
    public function paginate($dql, $page)
    {
        $this->setPage($page);    
        $paginator = new Paginator($dql);
        $paginator->getQuery()
            ->setFirstResult($this->firstResult())
            ->setMaxResults(self::ITEMBYPAGE)
        ;
        return $paginator;
    }



}