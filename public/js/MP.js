window.addEventListener('DOMContentLoaded', () => {

  const mp = new MercadoPago('TEST-8108aec2-986b-42d1-9b02-7b9f8d20038f');
  var errorDNI;

  const erroresObject = [
    {
        id: 205,
        error: "Ingresa el número de tu tarjeta",
        id_element: "form-checkout__cardNumber_error",

    },
    {
        id: 208,
        error: "Elige un mes",
        id_element: "form-checkout__cardExpirationMonth_error",
    },
    {
        id: 209,
        error: "Elige un año",
        id_element: "form-checkout__cardExpirationYear_error",
    },
    {
        id: 212,
        error: "Ingresa tu tipo de documento",
        id_element: "form-checkout__identificationType_error",
    },
    {
        id: 213,
        error: "Ingresa tu documento",
        id_element: "form-checkout__identificationNumber_error",
    },
    {
        id: 214,
        error: "Ingresa tu documento",
        id_element: "form-checkout__identificationNumber_error",
    },
    {
        id: 220,
        error: "Ingresa tu banco",
        id_element: "form-checkout__cardNumber_error",
    },
    {
        id: 221,
        error: "Ingresa Nombre y Apellido",
        id_element:  "form-checkout__cardholderName_error",
    },
    {
        id: 224,
        error: "Ingresa el código de seguridad",
        id_element: "form-checkout__securityCode_error",
    },
    {
        id: 'E301',
        error: "Ingresa el número de tu tarjeta",
        id_element: "form-checkout__cardNumber_error",
    },
    {
        id: 'E302',
        error: "Revisa el código de seguridad",
        id_element:  "form-checkout__securityCode_error",
    },
    {
        id: 316,
        error: "Ingresa un nombre válidos",
        id_element: "form-checkout__cardholderName_error",
    },
    {
        id: 322,
        error: "El tipo de documento es inválido.",
        id_element: "form-checkout__identificationType_error",
    },
    {
        id: 323,
        error: "Revisa tu documento",
        id_element: "form-checkout__identificationNumber_error",
    },
    {
        id: 324,
        error: "El documento es inválido",
        id_element: "form-checkout__identificationNumber_error",
    },
    {
        id: 325,
        error: "El mes es inválido",
        id_element:  "form-checkout__cardExpirationMonth_error",
    },
    {
        id: 326,
        error: "El año es invalido",
        id_element: "form-checkout__cardExpirationYear_error",
    },

  ];

  async function fIdentificationType() {
    const identificationTypes = await mp.getIdentificationTypes();
    return identificationTypes;
  }

  
  /**
   * identificationTypes => Tipo de Identiticacion/Documento
   * identificationNumber => Valor del input
   * array => Tipos de identificaciones que obtenida de fIdentificationType
   * 
   * */
  // function identificationValidation(event, array, identificationNumber, identificationTypes) {
  //   event.preventDefault();
  // }

  async function preve(evt) {
    return await evt.preventDefault();
  }

  function messageAlert(messageAlert) {
    return '<div class="alert alert-danger">'+ messageAlert +'</div>';
  }

  
  
  /*Funcion Encargada de Montar el Formulario*/
  function loadCardForm() {
    const cardForm = mp.cardForm({
      amount: $("#order-total").text().replace('$',''),
      autoMount: true,
      form: {
        id: "form-checkout",
        cardholderName: {
          id: "form-checkout__cardholderName",
          placeholder: "Titular de la tarjeta",
        },
        cardholderEmail: {
          id: "form-checkout__cardholderEmail",
          placeholder: "E-mail",
        },
        cardNumber: {
          id: "form-checkout__cardNumber",
          placeholder: "Número de la tarjeta",
        },
        cardExpirationMonth: {
          id: "form-checkout__cardExpirationMonth",
          placeholder: "MV",
        },
        cardExpirationYear: {
          id: "form-checkout__cardExpirationYear",
          placeholder: "AV",
        },
        securityCode: {
          id: "form-checkout__securityCode",
          placeholder: "Código de seguridad",
        },
        installments: {
          id: "form-checkout__installments",
          placeholder: "Cuotas",
        },
        identificationType: {
          id: "form-checkout__identificationType",
          placeholder: "Tipo de documento",
        },
        identificationNumber: {
          id: "form-checkout__identificationNumber",
          placeholder: "Número de documento",
        },
        issuer: {
          id: "form-checkout__issuer",
          placeholder: "Banco emisor",
        },
      },
      callbacks: {
        onFormMounted: error => {
          if (error) return console.warn("Form Mounted handling error: ", error);
          console.log("Form mounted");
        },
        onIdentificationTypesReceived: (error, identificationTypes) => {
                if (error) {
                  return console.warn('identificationTypes handling error: ', error)
                }
            },
        onSubmit: event => {
          event.preventDefault();
          const {
            paymentMethodId: payment_method_id,
            issuerId: issuer_id,
            cardholderEmail: email,
            amount,
            token,
            installments,
            identificationNumber,
            identificationType,
          } = cardForm.getCardFormData();

          fetch("/process_payment", {
            method: "POST",
            headers: {
              "Content-Type": "application/json",
            },
            body: JSON.stringify({
              token,
              issuer_id, 
              payment_method_id,
              transaction_amount: Number(amount),
              installments: Number(installments),
              description: "Descripción del producto",
              payer: {
                email,
                identification: {
                  type: identificationType,
                  number: identificationNumber,
                },
              },
            }),
          })
          .then(response =>{  
            return response.json();
          })
          .then(json_r => {
            console.log(json_r);
            if (json_r.status == "in_process" || json_r.status == "approved") {
               $(".qty").text("0");
            }

            $("#status").html("Status payment: " + json_r.status);
            $("#id").html("Id: " + json_r.id);
            $("#status_details").html("Details payment: " + json_r.status_detail);
            $("#details-payment").html($("#order-datail-payment").html());
            /*---------------------------------------------------------------------------------------------------*/
            $("#full_name").html(`Full Name: ${$("#person_check_out_name").val()} ${$("#person_check_out_lastName").val()}`);
            $('#address').html( `Address: ${$("#person_check_out_address").val()}`);
            $('#email').html( `E-mail: ${$("#person_check_out_email").text()}`);
            $('#telephone').html(`Telephone: ${$("#person_check_out_telephone").val()}`);
            $('#city').html(`City: ${$("#person_check_out_city").val()}`);
            $('#zip_code').html(`ZIP-Code: ${$("#person_check_out_zip_code").val()}`);
            if ($("#person_check_out_order_notes").val() !== "") {
                $('#other_comment').html(`Other Comment: ${$("#person_check_out_order_notes").val()}`);
            }

            setTimeout(function(){
              $("#row-section").html($(".container-payment-result").html());
            },500);
          });
        },
        onFetching: (resource) => {
          // console.log("Fetching resource: ", resource);
          // console.log(resource);
          let message;
          if (resource == 'cardToken') {
            fIdentificationType()
            .then(response => {
              console.log("max-length:" + response[0].max_length, "min-length:" + response[0].min_length);
              /*Verificar val en jquery*/
              const identificationTypes = $("#form-checkout__identificationType").val();
              const identificationNumber = $("#form-checkout__identificationNumber").val(); 
              console.log(identificationTypes, identificationNumber.length);
              let errorSpan = $("#form-checkout__identificationNumber_error").val(); 
              if (identificationTypes == "DNI") {
                console.log(identificationTypes);
                console.log(response);
                if (identificationNumber.length < response[0].min_length || identificationNumber.length > response[0].max_length) {
                  message = "Este valor debe contener 7 u 8 digitos";
                  document.getElementById("form-checkout__identificationNumber_error").innerHTML = errorSpan + messageAlert(message);
                  errorDNI = false;
                }

              }
              if (identificationTypes == "CI") {
                console.log(identificationTypes);
              }
              if (identificationTypes == "LC") {
                console.log(identificationTypes);
              }
              if (identificationTypes == "LE") {
                console.log(identificationTypes);
              }
              if (identificationTypes == "Otro") {
                console.log(identificationTypes);
              }
            });
          }
        },
        onCardTokenReceived: (error, token) => {
          console.log(error);
          if (error){  
              cleanErrorSpan();
              errores(erroresObject, error)
              .then(response => {
                console.log(response);
                if (response.length) {
                    //recorre los errores y los imprime en los span
                    response.forEach(errors =>{
                        // console.log(errors);
                        document.getElementById(errors.id_element).innerHTML = '<div class="alert alert-danger">' +  errors.error + "</div>";
                    });
                }
                return response;
              })
            if (!errorDNI) {
              preve();
            }
           return console.warn('Token handling error: ', error);
          }
          console.log('Token available: ', token);
        },
      },
    });
  }

  // objectErrores ---> objecto literal con errores de MP
  // codeErrMP ---> array que envia MP con los errores

  const errores = (objectErroes, codesErrMP) => {
    return new Promise((resolve, reject) => {
      let arrayErrores = [];
      // console.log(codesErrMP);
      codesErrMP.forEach(errorMP =>{
        // console.log(errorMP.code); //Errores MP
        objectErroes.filter(objErr =>{
          if (objErr.id == errorMP.code) {
            arrayErrores.push(objErr);
          }
        });
      });
      resolve(arrayErrores);
    });
  }
  

  function cleanErrorSpan() {
     document.getElementById("form-checkout__cardNumber_error").innerHTML = "";
     document.getElementById("form-checkout__cardExpirationMonth_error").innerHTML = "";
     document.getElementById("form-checkout__cardExpirationYear_error").innerHTML = "";
     document.getElementById("form-checkout__cardholderName_error").innerHTML = "";
     document.getElementById("form-checkout__identificationNumber_error").innerHTML = "";
     document.getElementById("form-checkout__securityCode_error").innerHTML = "";
  }


  document.getElementById('btn-place-order').addEventListener("click", function(){
    $(".container_check_out").fadeOut(500);
    $(".container-form-payment").fadeIn(500);
    loadCardForm();


  }, false);

  document.getElementById("form-checkout__back").addEventListener("click", function(){
    $(".container_check_out").fadeIn();
    $(".container-form-payment").fadeOut(500);
  }, false);



}, false)