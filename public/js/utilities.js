$(document).ready(function(){

	var base_url = location.origin;

	/**Button add item by cart*/
	$('body').on("click",".add-to-cart button", function(){
		let id = $(this).attr('id').split('-')[1];
		let url_controller = `${base_url}/product/cart/${id}`;
		
		/*Verifia si se encuenttra en la vista del producto*/
		let $_qty = $(".add-to-cart").has('.input-number') ? $('#qty-number-product').val() : 1;

		/*Verifica en caso de no ingreso de datos o valor 0*/
		if (!$_qty || $_qty == 0) {
			$_qty = 1;
		}

		let $qty = typeof $_qty === 'undefined' || $_qty === null ? 1 : $_qty;
		
		$.ajax({
			url: url_controller,
			data : {'qty': $qty},
			// dataType: "html",
			method: 'POST',
			success: function(html){
				$('#cart-icon-drop').html(html);
			},
			error: function(jqXHR, status, errThrow){
				console.log(status);
			},
		});
	});

	/**Button delete item by cart index*/

	$('body').on("click",".delete-to-cart button", function(){
		let key = $(this).attr('id').split('-')[1];
		let url_controller = `${base_url}/product/cart/delete/${key}`;
		$.ajax({
			url: url_controller,
			method: "POST",
			success: function(html){
				$('#row-section').html(html); /*Actualizacion index cart*/
				$('#cart-icon-drop').html($("#_hidden-cart-icon-delete").html()); /*Actualizacion de Carrito*/
				$('#breadcrumb').html($('#_hidden-breadcum-cart').html()); /*Actaulizacion de Bradcum con cantidad de Elementos en el carrito*/
				$('#error-message-flash').html($('#item-delete-alert').html()); /*Mensaje de Eliminacion elemento en index carrito*/
				$('#error-message-flash').fadeOut(3000); /*Demora en desaparecer mensaje de eliminacion*/
			},
			error: function(jqXHR, status, errThrow){
				console.log(status);
			},

		});
	});

	/**Button delete item by cart icon*/
	/*Verificacion pendiente*/
	/*Idea*/
 	/*__Realizar click en el btn, enviar header al controllador ProductController 
 	 indicando que es desde el icono la eliminacion 
 	 __ Renderizar el icono d nuevo*/
	$('body').on("click",".delete-to-cart-icon", function(){
		console.log('CLICK');
	});


	/*Filtro Categorias checkboxs*/
	$('body').on("click",".input-checkbox input:checkbox", function(){
		let url_controller = `${base_url}/product/category`;
		// console.log(url_controller);
		/*Toma array checkbox y lo asigna a variable para poder iterar*/
		let objCB = $("input[name='checkbox-category-aside']");
		let arr_categorys = new Array();
		/*AL hacer click en los checkbox, este verifica el selecionado y guarda ID en Array*/ 
		for (var i = 0; i < objCB.length; i++) {
			// console.log(objCB[0].checked);
			if (objCB[i].checked) {
				arr_categorys.push(objCB[i].id.split('-')[1]);
			}

		}
		// console.log(Object.values(arr_categorys));
		$.ajax({
			url : url_controller,
			method: "POST",
			data: {
				'arr_categorys': Object.values(arr_categorys),
			},
			headers : {
				'x-section' : 'aside',
			},
			success: function (html) {
				$('#store').html(html);
				$('#breadcrumb').html($('#_hidden-breadcum').html());

			},
			error : function(err) {
				console.log(err);
			}
		});
	});

	/*Realizar filtro con botones de paginado*/
	/*Colocar id en plantilla*/
	/*Hacer funcion checkbox checked*/

	$('body').on("click", "#store-filter-aside .store-pagination li", function() {
		//Mejorar esto
		let page_jqXHR = $(this).children()[0].id.split('-',3)[2];
		let url_controller = `${base_url}/product/category`;
		let objCB = $("input[name='checkbox-category-aside']");
		let arr_categorys = new Array();
		/*AL hacer click en los checkbox, este verifica el selecionado y guarda ID en Array*/ 
		for (var i = 0; i < objCB.length; i++) {
			if (objCB[i].checked) {
				arr_categorys.push(objCB[i].id.split('-')[1]);
			} 	
		}

		$.ajax({
			url : url_controller,
			method: "POST",
			data: {
				'arr_categorys': Object.values(arr_categorys),
				'page_jqXHR': parseInt(page_jqXHR),
			},
			headers : {
				'x-section' : 'aside',
			},
		})
		.done(function(html, textStatus, jqXHR){
			$('#store').html(html);
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR, textStatus, errorThrown);
		})
		;
	})



	// Utilizamos AJAX para SIMULAR envio de FORM
	$('body').on("change", '#product_form_parent', function(){
		let parent = $('#product_form_parent');
		let form = $("#box-content-principal form");
		data = {};
		// Simula el envio de un solo campo, loq ue hace que handleRequest 
		// detecte cambios en el Objeto , haciendo que se ejecute el evento
		// POST::SUBMIT de FORMEVENTS
		data[parent.attr('name')] = parent.val();
		$.ajax({
			url: form.attr('action'),
			method: form.attr('method'),
			data: data,
		})
		.done(function(html){
			console.log(html);
			$('#product_form_category').replaceWith(
		        $(html).find('#product_form_category')
		    );
		})
		.fail(function(jqXHR, textStatus, errorThrown){
			console.log(jqXHR.status);
			console.log(errorThrown);
		})
		;

	})

});

