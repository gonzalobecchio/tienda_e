const input = document.getElementById('product_form_images')
input.style.visibility = 'hidden';
const box_img_load = document.getElementById('box-img-load')

const renderDivFile = (val) => {
	console.log(URL.createObjectURL(val))
	return `<div class="col-md-12" style="border:'1px solid #00ff00'">
				<div class="col-md-2">
					<img src="${URL.createObjectURL(val)}" height="50" width="50">
				</div>
				<div class="col-md-10">
					<p>File Name : ${val.name}</p>
					<p>File Size: ${Math.round(val.size / 1024, -1)} Kilobytes</p>
				</div>
			</div>`
}

const filesLoadInput = () => {
	// const input = document.getElementById('product_form_images')
	input.onchange = () =>{
		console.log(Object.values(input.files))
		const files = Object.values(input.files)
		const renderImgsDiv = files.map((val, i) =>{
			console.log(val)
			return renderDivFile(val)
		})
		console.log(renderImgsDiv)
		document.getElementById('box-img-load').innerHTML = renderImgsDiv.join('')
	}
}

const filesOnLoadForm = () =>{
	const btn_examinar = document.getElementById('btn-examinar');
	btn_examinar.onclick = (e) =>{
		e.preventDefault()
		input.click()
	}
	if (!input.files.length) {
		const message = "<p class='no-selected-file-p'>No files selected . . .</p>"
		box_img_load.innerHTML = message
	}else{
		const files = Object.values(input.files)
		const renderImgsDiv = files.map((val) =>{
			return renderDivFile(val)
		})
		box_img_load.innerHTML = renderImgsDiv.join('')
	}
	

}

window.onload = () => {
	filesOnLoadForm()
	filesLoadInput()
}