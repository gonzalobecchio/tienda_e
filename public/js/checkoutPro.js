// Agrega credenciales de SDK

const base_url = location.origin;
const url = window.location;

const mp = new MercadoPago('TEST-8108aec2-986b-42d1-9b02-7b9f8d20038f', {
  locale: 'es-AR'
});

document.getElementById("btn-place-order").addEventListener("click", function(){
      // console.log(document.getElementById("person_check_out_name").value);
      let personal_data = {
         /*Realizar seguridad de datos*/
         person_check_out_name: document.getElementById("person_check_out_name").value,
         person_check_out_lastName: document.getElementById("person_check_out_lastName").value,
         person_check_out_address: document.getElementById("person_check_out_address").value,
         person_check_out_city: document.getElementById("person_check_out_city").value,
         person_check_out_zip_code: document.getElementById("person_check_out_zip_code").value,
         person_check_out_order_notes: document.getElementById("person_check_out_order_notes").value,
         person_check_out_address_number: document.getElementById("person_check_out_address_number").value,
         person_check_out_email: document.getElementById('person_check_out_email').options[document.getElementById('person_check_out_email').selectedIndex].text,
         person_check_out_dni: document.getElementById('person_check_out_dni').value,
         person_check_out_phone_area_code: document.getElementById('person_check_out_phone_area_code').value,
         person_check_out_phone_number: document.getElementById('person_check_out_phone_number').value,
      }

      let myInit = {
         body: JSON.stringify(personal_data),
         method: 'PUT',
         headers: {
            'Content-Type': 'application/json',
            'Authorization':  'Bearer TEST-4515115309737584-091414-ac8f240e204d7a5f251fe90fc990a703-824230212',
         }, 
      }
     // Inicializa el checkout
     fetch("/create_preference", myInit)
     .then(response => {
        return response.json();
     })
     .then(preference => {
        console.log(preference);
        createCheckOut(preference.id); 
        checkout.open(); 
     });
   }, false);


function createCheckOut(preferenceid) {
 const checkout = mp.checkout({
    preference: {
      id: preferenceid,
    },
    autoOpen: true,
  });

}











